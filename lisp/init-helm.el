(use-package helm
  :diminish helm-mode
  :bind (("C-c h" . helm-command-prefix)
         ("M-x" . helm-M-x)
         ("C-x b" . helm-mini)
         ("M-i" . helm-imenu)
         ("C-x C-f" . helm-find-files))

  :init
  (setq helm-prevent-escaping-from-minibuffer t
        helm-bookmark-show-location t
        helm-display-header-line nil
        helm-split-window-in-side-p t
        helm-always-two-windows t
        helm-echo-input-in-header-line t
        helm-imenu-execute-action-at-once-if-one nil)

  ;; fuzzy matching setting
  (setq helm-M-x-fuzzy-match t
        helm-apropos-fuzzy-match t
        helm-file-cache-fuzzy-match t
        helm-imenu-fuzzy-match t
        helm-lisp-fuzzy-completion t
        helm-recentf-fuzzy-match t
        helm-semantic-fuzzy-match t
        helm-buffers-fuzzy-matching t)

  ;; helm-locate uses es (from everything on windows, which doesnt like fuzzy)
  (setq helm-locate-fuzzy-match (executable-find "locate"))

  (evil-leader/set-key
    "<f1>" 'helm-apropos
    "bb"   'helm-mini
    "Cl"   'helm-colors
    "ff"   'helm-find-files
    "fL"   'helm-locate
    "fr"   'helm-recentf
    "hb"   'helm-filtered-bookmarks
    "hi"   'helm-imenu
    "hl"   'helm-resume
    "hm"   'helm-man-woman
    "hp"   'helm-browse-project
    "ry"   'helm-show-kill-ring
    "rr"   'helm-register
    "rm"   'helm-all-mark-rings)

  ;; Add minibuffer history with `helm-minibuffer-history'
  (bind-key "C-c C-l" 'helm-minibuffer-history minibuffer-local-map)

  :config
  (require 'helm-config)
  (custom-set-variables '(helm-command-prefix-key "C-c h"))
  (helm-autoresize-mode 1)

  ;; from http://www.reddit.com/r/emacs/comments/2z7nbv/lean_helm_window/
  (defvar helm-source-header-default-background (face-attribute 'helm-source-header :background))
  (defvar helm-source-header-default-foreground (face-attribute 'helm-source-header :foreground))
  (defvar helm-source-header-default-box (face-attribute 'helm-source-header :box))
  (defvar helm-source-header-default-height (face-attribute 'helm-source-header :height))

  (helm-mode +1)

  ;; alter helm-bookmark key bindings to be simpler
  ;; (defun simpler-helm-bookmark-keybindings ()
  ;;   (bind-keys :map  helm-bookmark-map
  ;;              ("C-d" . helm-bookmark-run-delete)
  ;;              ("C-e" . helm-bookmark-run-edit)
  ;;              ("C-f" . helm-bookmark-toggle-filename)
  ;;              ("C-o" . helm-bookmark-run-jump-other-window)
  ;;              ("C-/" . helm-bookmark-help)))
  ;; (add-hook 'helm-mode-hook 'simpler-helm-bookmark-keybindings)

  ;; Swap default TAB and C-z commands.
  (bind-keys :map helm-map
             ("<tab>" . helm-execute-persistent-action)
             ("TAB" . helm-execute-persistent-action)
             ("C-z" . helm-select-action))

  (use-package helm-flx
    :init
    (setq helm-flx-for-helm-find-files nil)

    :config
    (helm-flx-mode))

  (use-package helm-descbinds
    :bind ("C-h b" . helm-descbinds)

    :init
    (setq helm-descbinds-window-style 'split)
    (add-hook 'helm-mode-hook 'helm-descbinds-mode)
    (evil-leader/set-key "?" 'helm-descbinds))

  (use-package helm-ls-git
    :defer t

    :init
    (evil-leader/set-key
      "hg" 'helm-ls-git-ls))

  (use-package helm-mode-manager
    :defer t

    :init
    (evil-leader/set-key
      "hM"    'helm-switch-major-mode
      "hm"    'helm-disable-minor-mode
      "h C-m" 'helm-enable-minor-mode)))

(provide 'init-helm)
