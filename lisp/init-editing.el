(use-package undo-tree
  :diminish undo-tree-mode

  :config
  (global-undo-tree-mode))

(use-package expand-region
  :bind (("C-=" . er/expand-region)
         ("C-c =" . er/expand-region))

  :init
  (evil-leader/set-key "v" 'er/expand-region)
  (setq expand-region-contract-fast-key "V"
        expand-region-reset-fast-key "r"))

(use-package avy
  :bind (("C-'" . avy-goto-char-2)
         ("C-c '" . avy-goto-char-2)
         ("M-'" . avy-goto-word-or-subword-1))

  :init
  (evil-leader/set-key
    "SPC" 'avy-goto-word-or-subword-1
    "y" 'avy-goto-line)

  (seartipy/declare-prefixes "j" "avy")
  (evil-leader/set-key
    "jb" 'avy-pop-mark
    "jj" 'evil-avy-goto-char
    "jJ" 'evil-avy-goto-char-2
    "jl" 'evil-avy-goto-line
    "jL" 'evil-avy-goto-char-in-line
    "jw" 'evil-avy-goto-word-or-subword-1)

  (setq avy-keys (number-sequence ?a ?z)
        avy-style 'pre
        avy-all-windows nil
        avy-background t)

  :config
  (avy-setup-default)
  (evil-leader/set-key "`" 'avy-pop-mark))

(global-unset-key (kbd "M-c"))
(use-package multiple-cursors
  :bind (
         ("M-c M-c" . mc/edit-lines)
         ("M-c M-e" . mc/edit-ends-of-lines)
         ("M-c M-a" . mc/edit-beginnings-of-lines)

         ("C-<"     . mc/mark-previous-like-this)
         ("C-c <"   . mc/mark-previous-like-this)

         ("C->"     . mc/mark-next-like-this)
         ("C-c >"   . mc/mark-next-like-this)

         ("M-3"     . mc/mark-next-like-this)
         ("M-4"     . mc/mark-previous-like-this)
         ("M-#" . mc/unmark-next-like-this)
         ("M-$" . mc/unmark-previous-like-this)

         ("M-c a"   . mc/mark-all-like-this)
         ("M-c d"   . mc/mark-all-dwim)
         ("M-c f"   . mc/mark-all-symbols-like-this-in-defun)
         ("M-c h"   . mc-hide-unmatched-lines-mode)
         ("M-c i"   . mc/insert-numbers)
         ("M-c r"   . mc/mark-all-in-region)
         ("M-c s"   . mc/mark-all-symbols-like-this)
         ("M-c w"   . mc/mark-all-words-like-this)
         ("M-c M-r"   . mc/reverse-regions)
         ("M-c M-s" . mc/sort-regions)

         ("C-S-<mouse-1>" . mc/add-cursor-on-click))

  :init
  (custom-set-variables '(mc/always-run-for-all t)))

(use-package move-dup
  :defer t
  :bind (("M-S-<down>" . md/move-lines-down)
         ("s-d" . md/duplicate-down)
         ("M-s-<down>" . md/duplicate-down)
         ("M-s-<up>" . md/duplicate-up)
         ("M-S-<up>" . md/move-lines-up)
         ("s-D" . md/duplicate-up))

  :init
  (evil-leader/set-key
    "xj" 'md/duplicate-down
    "xk" 'md/duplicate-up
    "xJ" 'md/move-lines-down
    "xK" 'md/move-lines-up))

(use-package easy-kill
  :defer t

  :init
  (bind-key [remap kill-ring-save] 'easy-kill))

;;repace newlines with spaces
(use-package unfill :defer t)

(use-package editorconfig
  :diminish editorconfig-mode
  :defer t

  :init
  (add-hook 'prog-mode-hook #'editorconfig-mode))

(use-package lorem-ipsum
  :defer t

  :init
  (evil-leader/set-key
    "ill" 'lorem-ipsum-insert-list
    "ilp" 'lorem-ipsum-insert-paragraphs
    "ils" 'lorem-ipsum-insert-sentences))

(use-package smooth-scrolling
  :disabled t

  :config
  (smooth-scrolling-mode))

(use-package back-button
  :diminish back-button-mode

  :init
  (add-hook 'after-init-hook 'back-button-mode))

(use-package multifiles
  :bind ("C-!" . mf/mirror-region-in-multifile))

(use-package fix-word
  :defer t

  :init
  (evil-leader/set-key
    "xwu" 'fix-word-upcase
    "xwd" 'fix-word-downcase
    "xwc" 'fix-word-capitalize))

(use-package iedit
  :diminish iedit-mode)

(use-package goto-chg
  :bind (("C-." . goto-last-change)
         ("C-c ." . goto-last-change)
         ("C-," . goto-last-change-reverse)
         ("C-c ," . goto-last-change-reverse))
  :init
  (evil-leader/set-key
    "." 'goto-last-change
    "," 'goto-last-change-reverse))

(provide 'init-editing)
