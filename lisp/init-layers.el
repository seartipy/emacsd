(defun layer-selected-p (layer)
  (member layer seartipy-layers))

(dolist (layer seartipy-layers)
  (unless (member layer seartipy-supported-layers)
    (warn (format "%s layer not supported" layer))))

(require 'init-programming)
(if (or (layer-selected-p 'clojure)
        (layer-selected-p 'elisp))
    (require 'init-lispish))

(if (layer-selected-p 'clojure)
    (require 'init-clojure))
(if (layer-selected-p 'web)
    (require 'init-web))
(if (layer-selected-p 'scala)
    (require 'init-scala))
(if (layer-selected-p 'python)
    (require 'init-python))
(if (layer-selected-p 'cpp)
    (require 'init-cpp))
(if (layer-selected-p 'haskell)
    (require 'init-haskell))
(if (layer-selected-p 'elisp)
    (require 'init-elisp))

(if (layer-selected-p 'markdown)
    (require 'init-markdown))

(if (layer-selected-p 'shell)
    (require 'init-shell))
(if (layer-selected-p 'term)
    (require 'init-term))

(if (layer-selected-p 'tools)
    (require 'init-tools))

(if (layer-selected-p 'os)
    (require 'init-os))

(if (layer-selected-p 'themes)
    (require 'init-themes))
(if (layer-selected-p 'ui)
    (require 'init-ui))

(provide 'init-layers)
