(use-package emamux
  :defer t
  :if (not *is-a-windows*)
  :init
  (seartipy/declare-prefix "ot" "tmux")

  (evil-leader/set-key
    "ots" 'emamux:send-command
    "oty" 'emamux:yank-from-list-buffers)

  :config
  (when emamux:in-tmux-p
    (evil-leader/set-key
      "otr" 'emamux:run-command
      "otR" 'emamux:run-last-command
      "otS" 'emamux:run-region
      "oti" 'emamux:inspect-runner
      "otk" 'emamux:close-panes
      "otc" 'emamux:interrupt-runner
      "otK" 'emamux:clear-runner-history
      "otn" 'emamux:new-window
      "otC" 'emamux:clone-current-frame
      "ot2" 'emamux:split-window
      "ot3" 'emamux:split-window-horizontally)))

(use-package fasd
  :defer t
  :init
  (seartipy/declare-prefix "fa" "fasd-find")
  (defun fasd-find-file-only ()
    (interactive)
    (fasd-find-file -1))

  (defun fasd-find-directory-only ()
    (interactive)
    (fasd-find-file 1))

  (evil-leader/set-key
    "fad" 'fasd-find-directory-only
    "faf" 'fasd-find-file-only
    "fas" 'fasd-find-file)

  (setq fasd-enable-initial-prompt nil)

  :config
  (global-fasd-mode 1))

(use-package tmux
  :ensure nil
  :bind (("<S-left>" . tmux-nav-left)
         ("<S-down>" . tmux-nav-down)
         ("<S-up>" . tmux-nav-up)
         ("<S-right>" . tmux-nav-right))
  :preface
  (defun tmux-navigate (direction)
    (let
        ((cmd (concat "windmove-" direction)))
      (condition-case nil
          (funcall (intern cmd))
        (error
         (tmux-command direction)))))

  (defun tmux-command (direction)
    (shell-command-to-string
     (concat "tmux select-pane -"
             (tmux-direction direction))))

  (defun tmux-direction (direction)
    (upcase
     (substring direction 0 1)))

  (defun tmux-nav-left ()
    (interactive)
    (tmux-navigate "left"))

  (defun tmux-nav-right ()
    (interactive)
    (tmux-navigate "right"))

  (defun tmux-nav-up ()
    (interactive)
    (tmux-navigate "up"))

  (defun tmux-nav-down ()
    (interactive)
    (tmux-navigate "down"))

  :init
  (provide 'tmux))

(use-package company-shell
  :defer t

  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends '(company-shell :with company-capf))))

(use-package sh-script
  :defer t

  :init
  (evil-leader/set-key-for-mode 'sh-mode
    "\\" 'sh-backslash-region)

  ;; Use sh-mode when opening `.zsh' files, and when opening Prezto runcoms.
  (dolist (pattern '("\\.zsh\\'"
                     "zlogin\\'"
                     "zlogout\\'"
                     "zpreztorc\\'"
                     "zprofile\\'"
                     "zshenv\\'"
                     "zshrc\\'"))
    (add-to-list 'auto-mode-alist (cons pattern 'sh-mode)))

  (defun seartipy/setup-shell ()
    (when (and buffer-file-name
               (string-match-p "\\.zsh\\'" buffer-file-name))
      (sh-set-shell "zsh"))
    (company-mode))
  (add-hook 'sh-mode-hook 'seartipy/setup-shell))

(provide 'init-term)
