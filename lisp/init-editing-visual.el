(use-package whitespace-cleanup-mode
  :diminish whitespace-cleanup-mode
  :defer t

  :init
  (add-hook 'after-init-hook 'global-whitespace-cleanup-mode)
  (dolist (hook '(special-mode-hook
                  Info-mode-hook
                  eww-mode-hook
                  term-mode-hook
                  eshell-mode-hook
                  comint-mode-hook
                  compilation-mode-hook
                  magit-popup-mode-hook
                  minibuffer-setup-hook))
    (add-hook hook (lambda ()
                     (setq show-trailing-whitespace nil))))
  (bind-key [remap just-one-space] 'cycle-spacing))

(use-package origami
  :defer t

  :init
  (seartipy/declare-prefixes "z" "folding")
  (evil-leader/set-key
    "za" 'origami-forward-toggle-node
    "zc" 'origami-close-node
    "zC" 'origami-close-node-recursively
    "zO" 'origami-open-node-recursively
    "zo" 'origami-open-node
    "zr" 'origami-open-all-nodes
    "zm" 'origami-close-all-nodes
    "zs" 'origami-show-only-node
    "zn" 'origami-next-fold
    "zp" 'origami-previous-fold
    "zR" 'origami-reset
    "z TAB" 'origami-recursively-toggle-node)
  (add-hook 'prog-mode-hook 'global-origami-mode))

;; similar to vim's f command
(use-package jump-char
  :bind (("M-=" . jump-char-forward)
         ("M-+" . jump-char-backward))
  :init
  (use-package ace-jump-mode :defer t))

(use-package highlight-escape-sequences
  :init
  (add-hook 'after-init-hook 'hes-mode))

(use-package hl-todo
  :defer t

  :config
  (add-hook 'prog-mode-hook 'hl-todo-mode))

(use-package highlight-numbers
  :defer t

  :config
  (add-hook 'prog-mode-hook 'highlight-numbers-mode))

(use-package highlight-symbol :defer t)

(use-package command-log-mode
  :commands global-command-log-mode

  :init
  (evil-leader/set-key "aL" #'global-command-log-mode)

  :config
  (setq clm/log-command-exceptions* (append clm/log-command-exceptions*
                                            '(evil-next-line
                                              evil-previous-line
                                              evil-forward-char
                                              evil-backward-char))
        command-log-mode-auto-show t))

(use-package page-break-lines
  :diminish page-break-lines-mode
  :defer t

  :init
  (add-hook 'emacs-lisp-mode-hook 'page-break-lines-mode)
  (add-hook 'sh-mode-hook 'page-break-lines-mode))

(use-package focus :defer t)
(use-package writeroom-mode :defer t)

(use-package hideshow
  :defer t
  :init
  (seartipy/declare-prefixes "Z" "hideshow")

  (evil-leader/set-key
    "Za" 'hs-toggle-hiding
    "Zc" 'hs-hide-block
    "Zo" 'hs-show-block
    "Zr" 'hs-show-all
    "Zm" 'hs-hide-all))

(provide 'init-editing-visual)
