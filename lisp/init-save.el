;;;; saving emacs session

(use-package persp-mode
  :diminish persp-mode
  :unless seartipy-dashboard

  :init
  (setq persp-auto-resume-time 1
        persp-nil-name "Default"
        persp-reset-windows-on-nil-window-conf nil
        persp-set-last-persp-for-new-frames nil)
  (custom-set-variables '(persp-keymap-prefix (kbd "C-c l")))

  :config
  (evil-leader/set-key
    "l" (lookup-key persp-mode-map (kbd "C-c l")))
  (persp-mode))

(use-package savehist
  :init
  (setq enable-recursive-minibuffers t ; Allow commands in minibuffers
        history-length 1000
        savehist-additional-variables '(mark-ring
                                        global-mark-ring
                                        kill-ring
                                        search-ring
                                        regexp-search-ring
                                        extended-command-history)
        savehist-autosave-interval 60)

  :config
  (savehist-mode t))

(use-package saveplace
  :init
  (setq save-place t)

  :config
  (when (fboundp 'save-place-mode)
    (save-place-mode)))

(use-package recentf
  :init

  (setq recentf-max-saved-items 100
        recentf-auto-save-timer (run-with-idle-timer 600 t 'recentf-save-list)
        recentf-exclude '("/tmp/" "/ssh:"))

  :config
  (add-to-list 'recentf-exclude (file-truename no-littering-var-directory))
  (add-to-list 'recentf-exclude (file-truename no-littering-etc-directory))
  (add-to-list 'recentf-exclude (file-truename (concat user-emacs-directory "elpa/")))
  (recentf-mode +1))



(use-package dashboard
  :if seartipy-dashboard

  :init
  (setq dashboard-items '((recents  . 5)
                          (projects . 5)))

  :config
  (dashboard-setup-startup-hook))

(provide 'init-save)
