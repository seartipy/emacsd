(use-package evil
  :if (not (eq seartipy-default-editing-style 'emacs))
  :init
  ;; (when (eq seartipy-default-editing-style 'emacs)
  ;;   (setq evil-default-state 'emacs))

  (setq evil-normal-state-cursor '("DarkGoldenrod2" box))
  (setq evil-insert-state-cursor '("chartreuse3" (bar . 4)))
  (setq evil-hybrid-state-cursor '("SkyBlue2" (bar . 4)))
  (setq evil-emacs-state-cursor '("SkyBlue2" box))
  (setq evil-replace-state-cursor '("chocolate" (hbar . 4)))
  (setq evil-visual-state-cursor '("gray" (hbar . 4)))
  (setq evil-motion-state-cursor '("plum3" hollow))

  :config
  ;; (when (eq seartipy-default-editing-style 'emacs)
  ;;   (setq evil-emacs-state-modes (append evil-insert-state-modes evil-emacs-state-modes))
  ;;   (setq evil-insert-state-modes nil)

  ;;   (setq evil-emacs-state-modes (append evil-motion-state-modes evil-emacs-state-modes))
  ;;   (setq evil-motion-state-modes nil))

  (use-package hybrid-mode
    :ensure nil
    :diminish hybrid-mode " Ⓔh"

    :init
    (with-eval-after-load 'spaceline
      (setq spaceline-evil-state-faces(cons '(hybrid . spaceline-evil-insert) spaceline-evil-state-faces)))
    (custom-set-variables '(hybrid-mode-default-state 'hybrid))

    :config
    (when (eq seartipy-default-editing-style 'hybrid)
      (hybrid-mode)))

  (use-package evil-escape
    :diminish evil-escape-mode

    :init
    (evil-escape-mode))

  ;; 2-char searching ala vim-sneak & vim-seek, for evil-mode and Emacs
  (use-package evil-snipe
    :diminish evil-snipe-local-mode

    :init
    (evil-snipe-mode)
    (evil-snipe-override-mode))

  ;; you will be surrounded (surround.vim for evil, the extensible vi layer)
  (use-package evil-surround
    :config
    (global-evil-surround-mode)
    (evil-define-key 'visual evil-surround-mode-map "s" 'evil-surround-region)
    (evil-define-key 'visual evil-surround-mode-map "S" 'evil-substitute))

  ;; Start a * or # search from the visual selection
  (use-package evil-visualstar
    :bind (:map evil-visual-state-map
                ("*" . evil-visualstar/begin-search-forward)
                ("#" . evil-visualstar/begin-search-backward))

    :init
    (global-evil-visualstar-mode))

  ;; Motions and text objects for delimited arguments in Evil.
  (use-package evil-args
    :bind (:map evil-inner-text-objects-map
                ;; bind evil-args text objects
                ("a" . evil-inner-arg )
                :map evil-outer-text-objects-map
                ("a" . evil-outer-arg )

                :map evil-normal-state-map
                ;; bind evil-forward/backward-args
                ("L" . evil-forward-arg)
                ("H" . evil-backward-arg)
                ("L" . evil-forward-arg)
                :map evil-motion-state-map
                ("H" . evil-backward-arg)))

  ;; Press “%” to jump between matched tags in Emacs
  (use-package evil-matchit
    :config
    (global-evil-matchit-mode))

  (use-package evil-iedit-state
    :init
    (evil-leader/set-key "se" 'evil-iedit-state/iedit-mode))

  ;; indenting based textobjects
  (use-package evil-indent-plus
    :config
    (evil-indent-plus-default-bindings))

  (use-package evil-nerd-commenter
    :init
    (evil-leader/set-key
      ";"  'evilnc-comment-operator
      "cl" 'evilnc-comment-or-uncomment-lines
      "ci" 'evilnc-toggle-invert-comment-line-by-line
      "cp" 'evilnc-comment-or-uncomment-paragraphs
      "ct" 'evilnc-quick-comment-or-uncomment-to-the-line
      "cy" 'evilnc-copy-and-comment-lines))

  (use-package evil-anzu
    :diminish anzu-mode

    :init
    (setq anzu-search-threshold 1000
          anzu-cons-mode-line-p nil))

  (use-package evil-tutor :defer t)

  (evil-mode)

  ;; Make <escape> quit as much as possible
  (when (not (eq seartipy-default-editing-style 'emacs))
    (bind-key "<escape>" 'keyboard-escape-quit minibuffer-local-map )
    (bind-key "<escape>" 'keyboard-escape-quit minibuffer-local-ns-map)
    (bind-key "<escape>" 'keyboard-escape-quit minibuffer-local-completion-map)
    (bind-key "<escape>" 'keyboard-escape-quit minibuffer-local-must-match-map)
    (bind-key "<escape>" 'keyboard-escape-quit minibuffer-local-isearch-map)
    (bind-key "<escape>" 'keyboard-quit evil-visual-state-map)
    )
  )

(provide 'init-evil)
