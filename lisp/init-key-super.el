(use-package seartipy-super-keybindings
  :ensure nil
  :if seartipy-super-key
  :init
  (bind-key "s-'" 'evil-avy-goto-char-in-line)
  (bind-key "s-q" 'save-buffers-kill-terminal)
  (bind-key "s-v" 'yank)
  (bind-key "s-c" 'evil-yank)
  (bind-key "s-a" 'mark-whole-buffer)
  (bind-key "s-x" 'kill-region)
  (bind-key "s-w" 'delete-window)
  (bind-key "s-W" 'delete-frame)
  (bind-key "s-n" 'make-frame)
  (bind-key "s-z" 'undo-tree-undo)
  (bind-key "s-Z"  'undo-tree-redo)
  (bind-key "C-s-f" 'toggle-frame-fullscreen)

  (bind-key "s-=" (lambda () (interactive) (text-scale-increase 1)))
  (bind-key "s--" (lambda () (interactive) (text-scale-decrease 1)))
  (bind-key "s-0" (lambda () (interactive) (text-scale-increase 0)))
  (bind-key "s-s" (lambda ()
                    (interactive)
                    (call-interactively (key-binding "\C-x\C-s"))))

  (provide 'seartipy-super-keybindings))

(use-package seartipy-vscode-keybindings
  :ensure nil
  :if seartipy-super-key
  :init
  (bind-key "s-p" 'helm-projectile-find-file)
  (bind-key "s-P" 'helm-M-x)
  (bind-key "s-t" 'helm-imenu)
  (bind-key "s-\\" 'split-window-right)
  (bind-key "s-o" 'helm-find-file)
  (bind-key "s-F" 'helm-projectile-ag)
  (bind-key "s-X" 'paradox-list-packages)
  (bind-key "s-H" 'query-replace-regexp)
  (bind-key "s-f" 'isearch-forward)
  (bind-key "s-F" 'query-replace)
  (bind-key "s-<up>" 'scroll-up-line)
  (bind-key "s-<down>" 'scroll-down-line)
  (bind-key "<home>" 'crux-move-beginning-of-line)
  (bind-key "<end>" 'move-end-of-line)
  (bind-key "s-<left>" 'crux-move-beginning-of-line)
  (bind-key "s-<right>" 'move-end-of-line)
  (bind-key "s-<home>" 'beginning-of-buffer)
  (bind-key "s-<end>" 'end-of-buffer)
  (bind-key "s-k" 'crux-kill-whole-line)
  (bind-key "s-RET" 'crux-smart-open-line)
  (bind-key "s-S-RET" 'crux-smart-open-line-above)
  (bind-key "s-<up>" 'beginning-of-buffer)
  (bind-key "s-<down>" 'end-of-buffer)
  (bind-key "s-K s-[" 'origami-close-all-nodes)
  (bind-key "s-K s-]" 'origami-open-all-nodes)
  (bind-key "s-/" 'comment-dwim)
  (bind-key "s-N" 'make-frame-command)
  (bind-key "s-S-<up>" 'select-scroll-up-line)
  (bind-key "s-S-<down>" 'select-scroll-down-line)
  ;; (bind-key "<s-S-<left>" 'select-begin
  ;; (bind-key "<s-S-<right>" 'select-end-line)

  (provide 'seartipy-vscode-keybindings))

;; (use-package simpleclip)

(provide 'init-key-super)
