(use-package git-gutter-fringe
  :defer t
  :diminish git-gutter-mode

  :init
  (setq git-gutter:hide-gutter t
        git-gutter:update-interval 3
        git-gutter:verbosity 0)
  (add-hook 'after-init-hook
            (lambda ()
              (require 'git-gutter-fringe)))
  :config
  (global-git-gutter-mode))

(use-package smerge-mode
  :defer t
  :diminish smerge-mode

  :init
  (defhydra hydra-smerge (:hint nil)
    "
 movement^^^^               merge action^^           other
 ---------------------^^^^  -------------------^^    -----------
 [_n_]^^    next hunk       [_b_] keep base          [_u_] undo
 [_N_/_p_]  prev hunk       [_m_] keep mine          [_r_] refine
 [_j_/_k_]  move up/down    [_a_] keep all           [_q_] quit
 ^^^^                       [_o_] keep other
 ^^^^                       [_c_] keep current
 ^^^^                       [_C_] combine with next"
    ("n" smerge-next)
    ("p" smerge-prev)
    ("N" smerge-prev)
    ("j" evil-next-line)
    ("k" evil-previous-line)
    ("a" smerge-keep-all)
    ("b" smerge-keep-base)
    ("m" smerge-keep-mine)
    ("o" smerge-keep-other)
    ("c" smerge-keep-current)
    ("C" smerge-combine-with-next)
    ("r" smerge-refine)
    ("u" undo-tree-undo)
    ("q" nil :exit t))

  (seartipy/declare-prefixes "gr" "smerge")
  (evil-leader/set-key
    "gr" 'hydra-smerge/body))

(use-package magit
  :commands (magit-blame-mode
             magit-commit-popup
             magit-diff-popup
             magit-fetch-popup
             magit-log-popup
             magit-pull-popup
             magit-push-popup
             magit-status)
  :bind (("C-x g" . magit-status)
         ("C-x M-g" . magit-dispatch-popup)
         :map magit-status-mode-map
         ("C-M-<up>" . magit-section-up))
  :init
  (setq-default
   magit-process-popup-time 10
   magit-diff-refine-hunk t)

  (seartipy/declare-prefixes "gd" "diff")

  (evil-leader/set-key
    "gc" 'magit-commit-popup
    "gC" 'magit-checkout
    "gd" 'magit-diff-popup
    "ge" 'magit-ediff-compare
    "gE" 'magit-ediff-show-working-tree
    "gf" 'magit-fetch-popup
    "gF" 'magit-pull-popup
    "gi" 'magit-init
    "gl" 'magit-log-popup
    "gL" 'magit-log-buffer-file
    "gP" 'magit-push-popup
    "gs" 'magit-status
    "gS" 'magit-stage-file
    "gU" 'magit-unstage-file)

  :config
  (fullframe magit-status magit-mode-quit-window)

  (use-package git-commit
    :defer t

    :init
    (add-hook 'git-commit-mode-hook 'goto-address-mode)
    (when *is-a-mac*
      (add-hook 'magit-mode-hook (lambda () (local-unset-key [(meta h)]))))))

(use-package git-messenger
  :bind ("C-x v p" . git-messenger:popup-message))

(use-package gitignore-mode :defer t)
(use-package gitconfig-mode :defer t)
(use-package gitattributes-mode :defer t)
(use-package git-timemachine :defer t)
(use-package gist :defer t)
(use-package git-link :defer t)

(use-package browse-at-remote
  :defer t

  :init
  (seartipy/declare-prefixes "gh" "github")
  (evil-leader/set-key
    "gb" 'browse-at-remote))

(use-package ibuffer-vc
  :defer t
  :init
  (defun ibuffer-set-up-preferred-filters ()
    (ibuffer-vc-set-filter-groups-by-vc-root)
    (unless (eq ibuffer-sorting-mode 'filename/process)
      (ibuffer-do-sort-by-filename/process)))

  (add-hook 'ibuffer-hook 'ibuffer-set-up-preferred-filters))

(use-package helm-gitignore
  :defer t

  :init
  (evil-leader/set-key "gI" 'helm-gitignore))

(provide 'init-git)
