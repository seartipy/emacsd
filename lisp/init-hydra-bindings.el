;;;; hydra bindings

(defhydra hydra-move-dup ()
  "move/duplicate"
  ("k" md/move-lines-up "Move Up")
  ("<up>" md/move-lines-up "Move Up")

  ("j" md/move-lines-down "Move Down")
  ("<down>" md/move-lines-down "Move Down")

  ("K" md/duplicate-up "Duplicate Above")
  ("<M-up>" md/duplicate-up "Duplicate Above")

  ("J" md/duplicate-down "Duplicate Below")
  ("<M-down>" md/duplicate-down "Duplicate Below")

  ("<ESC>" nil)
  ("q" nil))

(defhydra hydra-font-zoom ()
  "font-zoom"
  ("+" text-scale-increase "in")
  ("-" text-scale-decrease "out")
  ("=" (text-scale-set 0))
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-splitter ()
  "splitter"
  ("h" hydra-move-splitter-left "left")
  ("j" hydra-move-splitter-down "down")
  ("k" hydra-move-splitter-up "up")
  ("l" hydra-move-splitter-right "right")

  ("<left>" hydra-move-splitter-left "left")
  ("<up>" hydra-move-splitter-up "up")
  ("<right>" hydra-move-splitter-right "right")
  ("<down>" hydra-move-splitter-down "down")

  ("b" balance-windows)

  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-movement ()
  "movement"
  ("n" next-line "next")
  ("p" previous-line "prev")

  ("h" scroll-left "left")
  ("j" scroll-down "down")
  ("k" scroll-up "up")
  ("l" scroll-right "right")

  ("<left>" scroll-left "left")
  ("<down>" scroll-down "down")
  ("<up>" scroll-up "up")
  ("<right>" scroll-right "right")

  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-other-window ()
  "other-window"
  ("n" (scroll-other-window 1) "next line")
  ("p" (scroll-other-window -1) "prev line")
  ("N" scroll-other-window "next page")
  ("P" (scroll-other-window '-) "prev page")

  ("<down>" (scroll-other-window 1) "next line")
  ("<up>" (scroll-other-window -1) "prev line")
  ("<M-down>" scroll-other-window "next page")
  ("<M-up>" (scroll-other-window '-) "prev page")

  ("a" (beginning-of-buffer-other-window 0) "begin")
  ("e" (end-of-buffer-other-window 0) "end")

  ("<ESC>" nil "quit")
  ("q" nil "quit"))

;; https://github.com/abo-abo/hydra/wiki/multiple-cursors
(defhydra hydra-multiple-cursors (:hint nil)
  "
     ^Up^            ^Down^        ^Other^
----------------------------------------------
[_p_]   Next    [_n_]   Next    [_l_] Edit lines
[_P_]   Skip    [_N_]   Skip    [_a_] Mark all
[_M-p_] Unmark  [_M-n_] Unmark  [_r_] Mark by regexp
^ ^             ^ ^             [_q_] Quit
"
  ("l" mc/edit-lines :exit t)
  ("a" mc/mark-all-like-this :exit t)
  ("n" mc/mark-next-like-this)
  ("N" mc/skip-to-next-like-this)
  ("M-n" mc/unmark-next-like-this)
  ("p" mc/mark-previous-like-this)
  ("P" mc/skip-to-previous-like-this)
  ("M-p" mc/unmark-previous-like-this)
  ("r" mc/mark-all-in-region-regexp :exit t)
  ("<mouse-1>" mc/add-cursor-on-click)
  ("<down-mouse-1>" ignore)
  ("<drag-mouse-1>" ignore)
  ("q" nil)
  ("<ESC>" nil "quit"))

;; https://github.com/abo-abo/hydra/wiki/Helm-2
(defhydra hydra-helm (:hint nil :color pink)
  "
                                                                          ╭──────┐
   Navigation   Other  Sources     Mark             Do             Help   │ Helm │
  ╭───────────────────────────────────────────────────────────────────────┴──────╯
        ^_k_^         _K_       _p_   [_m_] mark         [_v_] view         [_H_] helm help
        ^^↑^^         ^↑^       ^↑^   [_t_] toggle all   [_d_] delete       [_s_] source help
    _h_ ←   → _l_     _c_       ^ ^   [_u_] unmark all   [_f_] follow: %(helm-attr 'follow)
        ^^↓^^         ^↓^       ^↓^    ^ ^               [_y_] yank selection
        ^_j_^         _J_       _n_    ^ ^               [_w_] toggle windows
  --------------------------------------------------------------------------------
        "
  ("<tab>" helm-keyboard-quit "back" :exit t)
  ("<escape>" nil "quit")
  ("\\" (insert "\\") "\\" :color blue)
  ("h" helm-beginning-of-buffer)
  ("j" helm-next-line)
  ("k" helm-previous-line)
  ("l" helm-end-of-buffer)
  ("g" helm-beginning-of-buffer)
  ("G" helm-end-of-buffer)
  ("n" helm-next-source)
  ("p" helm-previous-source)
  ("K" helm-scroll-other-window-down)
  ("J" helm-scroll-other-window)
  ("c" helm-recenter-top-bottom-other-window)
  ("m" helm-toggle-visible-mark)
  ("t" helm-toggle-all-marks)
  ("u" helm-unmark-all)
  ("H" helm-help)
  ("s" helm-buffer-help)
  ("v" helm-execute-persistent-action)
  ("d" helm-persistent-delete-marked)
  ("y" helm-yank-selection)
  ("w" helm-toggle-resplit-and-swap-windows)
  ("f" helm-follow-mode))

(defhydra hydra-flycheck
  (:pre (progn (setq hydra-lv t) (flycheck-list-errors))
        :post (progn (setq hydra-lv nil) (quit-windows-on "*Flycheck errors*"))
        :hint nil)
  "Errors"
  ("f"  flycheck-error-list-set-filter                            "Filter")
  ("j"  flycheck-next-error                                       "Next")
  ("k"  flycheck-previous-error                                   "Previous")
  ("gg" flycheck-first-error                                      "First")
  ("G"  (progn (goto-char (point-max)) (flycheck-previous-error)) "Last")
  ("q"  nil))

(defhydra hydra-goto-line (goto-map ""
                                    :pre (linum-mode 1)
                                    :post (linum-mode -1))
  "goto-line"
  ("g" goto-line "go")
  ("m" set-mark-command "mark" :bind nil)
  ("q" nil "quit"))

(defhydra hydra-page (ctl-x-map "" :pre (widen))
  "page"
  ("]" forward-page "next")
  ("[" backward-page "prev")
  ("n" narrow-to-page "narrow" :bind nil :exit t)
  ("q" narrow-to-page "narrow" :bind nil :exit t)
  ("ESC" narrow-to-page "narrow" :bind nil :exit t))

(defhydra hydra-git-gutter (:body-pre (git-gutter-mode 1)
                                      :hint nil)
  "
Git gutter:
     ^_k_^        _s_tage hunk     _q_uit
     ^^↑^^        _r_evert hunk    _Q_uit and deactivate git-gutter
 _h_ ←   → _l_    _p_opup hunk
     ^^↓^^
     ^_j_^        set start _R_evision
"
  ("j" git-gutter:next-hunk)
  ("k" git-gutter:previous-hunk)
  ("h" (progn (goto-char (point-min))
              (git-gutter:next-hunk 1)))
  ("l" (progn (goto-char (point-min))
              (git-gutter:previous-hunk 1)))

  ("<down>" git-gutter:next-hunk)
  ("<up>" git-gutter:previous-hunk)
  ("<left>" (progn (goto-char (point-min))
                   (git-gutter:next-hunk 1)))
  ("<right>" (progn (goto-char (point-min))
                    (git-gutter:previous-hunk 1)))

  ("s" git-gutter:stage-hunk)
  ("r" git-gutter:revert-hunk)
  ("p" git-gutter:popup-hunk)
  ("R" git-gutter:set-start-revision)
  ("q" nil :color blue)
  ("Q" (progn (git-gutter-mode -1)
              ;; git-gutter-fringe doesn't seem to
              ;; clear the markup right away
              (sit-for 0.1)
              (git-gutter:clear))
   :color blue))

(defhydra hydra-hydras (:color blue :columns 4)
  ("l" hydra-move-dup/body "move/duplicate")
  ("m" hydra-movement/body "move around")
  ("o" hydra-other-window/body "other window")
  ("s" hydra-splitter/body "resize windows")
  ("z" hydra-font-zoom/body "zoom font")
  ("c" hydra-multiple-cursors/body "multiple cursors")
  ("h" hydra-helm/body "helm")
  ("e" hydra-flycheck/body "flycheck")
  ("p" hydra-page/body "page")
  ("g" hydra-git-gutter/body "git"))

(key-chord-define-global "hj" 'hydra-hydras/body)
(evil-leader/set-key "H" 'hydra-hydras/body)
(bind-key "M-l" 'hydra-hydras/body)

(provide 'init-hydra-bindings)
