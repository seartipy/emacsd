(use-package crux
  :bind (("C-<backspace>" . crux-kill-line-backwards)
         ("C-c <backspace>" . crux-kill-line-backwards)
         ("C-M-<backspace>" . crux-kill-whole-line)
         ("C-c M-<backspace>" . crux-kill-whole-line)
         ("M-o" . crux-smart-open-line)
         ;; ("M-O" . crux-smart-open-line-above) ;; does not work in terminal
         ("C-M-o" . crux-smart-open-line-above)) ;; C-M-o was split line
  :commands (crux-move-beginning-of-line)

  :init
  (bind-key [remap move-beginning-of-line] #'crux-move-beginning-of-line)
  (bind-key [(shift return)] #'crux-smart-open-line)
  (bind-key [(control shift return)] #'crux-smart-open-line-above)
  (bind-key [remap kill-whole-line] #'crux-kill-whole-line))

(evil-leader/set-key
  "nr" 'narrow-to-region
  "np" 'narrow-to-page
  "nf" 'narrow-to-defun
  "nw" 'widen)

;; Zap to char
(autoload 'zap-up-to-char "misc" "Kill up to, but not including ARGth occurrence of CHAR.")
(bind-key "M-z" 'zap-up-to-char)
(bind-key "M-Z" (lambda (char) (interactive "cZap to char: ") (zap-to-char 1 char)))

;; Transpose stuff with M-t
(global-unset-key (kbd "M-t")) ;; which used to be transpose-words
(bind-keys
 ("M-t l" . transpose-lines)
 ("M-t w" . transpose-words)
 ("M-t s" . transpose-sexps)
 ("M-t p" . transpose-params))

;; Vimmy alternatives to M-^ and C-u M-^
(bind-key "M-j" (lambda () (interactive) (join-line -1)))
(bind-key "M-J" (lambda () (interactive) (join-line 1)))

;; (bind-key "C-." 'set-mark-command)
;; (bind-key "C-x C-." 'pop-global-mark)

(bind-key "M-/" 'hippie-expand)

(provide 'init-editing-bindings)
