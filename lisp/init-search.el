;; Prefer g-prefixed coreutils version of standard utilities when available
(let ((gls (executable-find "gls")))
  (when gls (setq insert-directory-program gls)))

(setq-default grep-highlight-matches t
              grep-scroll-output t)

(when *is-a-mac*
  (setq-default locate-command "mdfind"))

(use-package ag
  :defer t

  :init
  (setq ag-highlight-search t
        ag-reuse-buffers t))

(use-package wgrep :defer t)

(use-package wgrep-ag
  :commands wgrep-ag-setup)

(use-package helm-ag
  :defer t
  :init
  (custom-set-variables
   '(helm-ag-use-aginore t)
   '(helm-ag-instert-at-point 'symbol))

  (evil-leader/set-key
    "sA" 'helm-ag
    "sa" 'helm-do-ag

    "sF" 'helm-ag-this-file
    "sf" 'helm-do-ag-this-file

    "sB" 'helm-ag-buffers
    "sb" 'helm-do-ag-buffers

    "sP" 'helm-ag-project-root
    "sp" 'helm-do-ag-project-root
    "s`" 'helm-ag-pop-stack))

(use-package helm-swoop
  :defer t

  :init
  (setq helm-swoop-split-with-multiple-windows t
        helm-swoop-split-direction 'split-window-vertically
        helm-swoop-speed-or-color t
        helm-swoop-use-fuzzy-match t
        helm-swoop-move-to-line-cycle t
        helm-swoop-split-window-function 'helm-default-display-buffer
        helm-swoop-pre-input-function (lambda () ""))

  (seartipy/declare-prefixes
    "sm" "helm-swoop")

  (evil-leader/set-key
    "ss"    'helm-swoop
    "sm'"   'helm-swoop-back-to-last-point

    "sms"   'helm-multi-swoop
    "smp"   'helm-multi-swoop-projectile
    "smm"   'helm-multi-swoop-current-mode
    "sma"   'helm-multi-swoop-all))

(provide 'init-search)
