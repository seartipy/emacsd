;;;; MELPA

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(setq load-prefer-newer t)

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(setq use-package-always-ensure t)
(setq use-package-verbose t)
(custom-set-variables '(use-package-inject-hooks t))

(eval-when-compile
  (require 'use-package))

;; needed by use-package
(use-package diminish)
(use-package bind-key)

(use-package auto-package-update :defer t)
(use-package paradox :defer t)

(defun seartipy/package-install-refresh-contents (&rest args)
  "On first use-package package install, do PACKAGE-REFRESH-CONTENTS.  ARGS ignored."
  (package-refresh-contents)
  (advice-remove 'package-install 'seartipy/package-install-refresh-contents))

(advice-add 'package-install :before 'seartipy/package-install-refresh-contents)

(provide 'init-melpa)
