;; Universal argument
(evil-leader/set-key "u" 'universal-argument)

;; shell command
(evil-leader/set-key "!" 'shell-command)



;;;; file

(defun seartipy/open-in-external-app ()
  "Open current file in external application."
  (interactive)
  (let ((file-path (if (eq major-mode 'dired-mode)
                       (dired-get-file-for-visit)
                     (buffer-file-name))))
    (if file-path
        (cond
         (*is-a-mac* (shell-command (format "open \"%s\"" file-path)))
         (*is-a-linux* (let ((process-connection-type nil))
                         (start-process "" nil "xdg-open" file-path))))
      (message "No file associated to this buffer."))))

(defun seartipy/show-and-copy-buffer-filename ()
  "Show the full path to the current file in the minibuffer."
  (interactive)
  (let ((file-name (buffer-file-name)))
    (if file-name
        (progn
          (message file-name)
          (kill-new file-name))
      (error "Buffer not visiting a file"))))

(defun seartipy/save-buffer ()
  "Save the current buffer."
  (interactive)
  (if (fboundp 'evil-write)
      (evil-write)
    (save-buffer)))

(defun seartipy/save-all-buffers ()
  "Save all buffers."
  (interactive)
  (if (fboundp 'evil-write-all)
      (evil-write-all)
    (save-some-buffers)))

(evil-leader/set-key
  "fg" 'rgrep
  "fj" 'dired-jump
  "fo" 'seartipy/open-in-external-app
  "fR" 'rename-current-buffer-file
  "fS" 'seartipy/save-buffer
  "fs" 'seartipy/save-all-buffers
  "fy" 'seartipy/show-and-copy-buffer-filename)



;;;; buffer

(use-package buffer-move
  :bind (("<C-S-up>" . buf-move-up)
         ("<C-S-down>" . buf-move-down)
         ("<C-S-left>" . buf-move-left)
         ("<C-S-right>" . buf-move-right))

  :init
  (evil-leader/set-key
    "bmh" 'buf-move-left
    "bmj" 'buf-move-down
    "bmk" 'buf-move-up
    "bml" 'buf-move-right))

(defun seartipy/switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(defun seartipy/kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (let (name (buffer-name))
    (when (yes-or-no-p (format "Killing all buffers except \"%s\" ? " buffer-file-name))
      (mapc 'kill-buffer (delq (current-buffer) (buffer-list)))
      (message "Buffers deleted!"))))

(defun seartipy/safe-revert-buffer ()
  "Prompt before reverting the file."
  (interactive)
  (revert-buffer nil nil))

(evil-leader/set-key
  "bd"  'kill-this-buffer
  "TAB" 'seartipy/switch-to-previous-buffer
  "bK"  'seartipy/kill-other-buffers
  "bR"  'seartipy/safe-revert-buffer
  "bw"  'read-only-mode)

(bind-key "C-c b" 'seartipy/switch-to-previous-buffer)
(bind-key "C-x C-b" 'ibuffer)



(evil-leader/set-key
  "hdb" 'describe-bindings
  "hdc" 'describe-char
  "hdf" 'describe-function
  "hdk" 'describe-key
  "hdp" 'describe-package
  "hdt" 'describe-theme
  "hdv" 'describe-variable
  "hn"  'view-emacs-news)

(evil-leader/set-key
  "xaa" 'align
  "xc"  'count-region
  "xdw" 'delete-trailing-whitespace
  "xtc" 'transpose-chars
  "xtl" 'transpose-lines
  "xtw" 'transpose-words
  "xU"  'upcase-region
  "xu"  'downcase-region)

;; applications
(evil-leader/set-key
  "ac"  'calc-dispatch
  "ad"  'dired
  "ap"  'list-processes
  "aP"  'proced
  "au"  'undo-tree-visualize)

;; errors
(evil-leader/set-key
  "en" 'next-error
  "ep" 'previous-error
  "eN" 'previous-error)

;; Compilation ----------------------------------------------------------------
;;(evil-leader/set-key "cc" 'helm-make-projectile)
(evil-leader/set-key "cC" 'compile)
(evil-leader/set-key "cr" 'recompile)

;; spell check  ---------------------------------------------------------------
(evil-leader/set-key
  "Sd" 'ispell-change-dictionary
  "Sn" 'flyspell-goto-next-error)



;;;; windows

(use-package ace-window
  :defer t

  :init
  (evil-leader/set-key
    "bM"  'ace-swap-window
    "wC"  'ace-delete-window
    "w <SPC>"  'ace-window)
  (setq aw-dispatch-always t))

;; When splitting window, show (other-buffer) in the new window
(defun seartipy/split-window-func-with-other-buffer (split-function)
  (lexical-let ((s-f split-function))
    (lambda ()
      (interactive)
      (funcall s-f)
      (set-window-buffer (next-window) (other-buffer)))))

;; rearrange split windows
(defun seartipy/split-window-horizontally-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-horizontally))))

(defun seartipy/split-window-vertically-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-vertically))))


;; window functions

(defun seartipy/switch-to-minibuffer-window ()
  "switch to minibuffer window (if active)"
  (interactive)
  (when (active-minibuffer-window)
    (select-window (active-minibuffer-window))))

(defun seartipy/split-window-below-and-focus ()
  "Split the window vertically and focus the new window."
  (interactive)
  (split-window-below)
  (windmove-down))

(defun seartipy/split-window-right-and-focus ()
  "Split the window horizontally and focus the new window."
  (interactive)
  (split-window-right)
  (windmove-right))

(defun seartipy/layout-triple-columns ()
  " Set the layout to triple columns. "
  (interactive)
  (delete-other-windows)
  (split-window-right)
  (split-window-right)
  (balance-windows))

(defun seartipy/layout-double-columns ()
  " Set the layout to double columns. "
  (interactive)
  (delete-other-windows)
  (split-window-right))

(defun seartipy/layout-triple-rows ()
  " Set the layout to triple columns. "
  (interactive)
  (delete-other-windows)
  (split-window-below)
  (split-window-below)
  (balance-windows))

(defun seratipy/layout-double-rows ()
  " Set the layout to double columns. "
  (interactive)
  (delete-other-windows)
  (split-window-below))

(use-package golden-ratio
  :diminish " ⓖ"
  :defer t)

(evil-leader/set-key
  "w@"  'seartipy/layout-double-rows
  "w#"  'seartipy/layout-triple-rows
  "w2"  'seartipy/layout-double-columns
  "w3"  'seartipy/layout-triple-columns

  "wb"  'seartipy/switch-to-minibuffer-window
  "wc"  'delete-window

  "wH"  'evil-window-move-far-left
  "wh"  'evil-window-left
  "wJ"  'evil-window-move-very-bottom
  "wj"  'evil-window-down
  "wK"  'evil-window-move-very-top
  "wk"  'evil-window-up
  "wL"  'evil-window-move-far-right
  "wl"  'evil-window-right

  "wo"  'other-frame
  "wu"  'winner-undo
  "wv"  'split-window-right

  "ws"  'split-window-below
  "wS"  'seartipy/split-window-below-and-focus
  "w-"  'split-window-below
  "wU"  'winner-redo
  "wV"  'seartipy/split-window-right-and-focus
  "w/"  'split-window-right
  "wd"  'delete-window
  "wD"  'delete-other-windows
  "ww"  'other-window
  "w="  'balance-windows

  "w_" 'seartipy/split-window-horizontally-instead
  "w|" 'seartipy/split-window-vertically-instead)

(use-package winum
  :init
  (evil-leader/set-key
    "`" 'winum-select-window-by-number
    "0" 'winum-select-window-0-or-10
    "1" 'winum-select-window-1
    "2" 'winum-select-window-2
    "3" 'winum-select-window-3
    "4" 'winum-select-window-4
    "5" 'winum-select-window-5
    "6" 'winum-select-window-6
    "7" 'winum-select-window-7
    "8" 'winum-select-window-8
    "9" 'winum-select-window-9)
  (setq winum-auto-setup-mode-line nil)

  :config
  (winum-mode))



;;;; windows

(bind-key "C-x C-2" (seartipy/split-window-func-with-other-buffer 'split-window-vertically))
(bind-key "C-x C-3" (seartipy/split-window-func-with-other-buffer 'split-window-horizontally))
(bind-key "\C-x|" 'seartipy/split-window-horizontally-instead)
(bind-key "\C-x_" 'seartipy/split-window-vertically-instead)
(bind-key "C-x o" 'ace-window)



;; Navigate window layouts with "C-c <left>" and "C-c <right>"
(winner-mode)
(windmove-default-keybindings)



(seartipy/declare-prefix "to" "other")
(evil-leader/set-key
  "toy" 'symon-mode
  "tof" 'focus-read-only-mode
  "toF" 'focus-mode
  "tow" 'writeroom-mode
  "tot" 'multi-term-dedicated-toggle
  "tog" 'golden-ratio-mode
  "ton" 'neotree-toggle

  "ts" 'smartparens-strict-mode
  "tS" 'smartparens-mode
  "thh" 'hs-minor-mode
  "thl" 'global-hl-line-mode
  "ths" 'highlight-symbol
  "tl" 'linum-mode
  "tM" 'toggle-frame-fullscreen
  "tm" 'toggle-frame-maximized
  "td" 'toggle-debug-on-error
  "tv" 'smooth-scrolling-mode
  "tCd" 'rainbow-delimeters-mode
  "tw" 'whitespace-mode
  "t C-I" 'aggressive-indent-mode)

;; Convenient binding for vc-git-grep
(bind-key "C-x v f" 'vc-git-grep)

;; Create new frame
(bind-key "C-x C-n" 'make-frame-command)

;; Revert without any fuss
(bind-key "M-ESC" (lambda () (interactive) (revert-buffer t t)))

;; ;; Edit file with sudo
;; (bind-key (kbd "M-s e") 'sudo-edit)

;; Help should search more than just commands
(bind-key "<f1> a" 'apropos)

;; Yank selection in isearch
(bind-key "C-o" 'isearch-yank-x-selection isearch-mode-map)

;; ;; Toggle quotes
;; (bind-key (kbd "C-\"") 'toggle-quotes)

;; Display and edit occurances of regexp in buffer
(bind-key "C-c o" 'occur)
(bind-key "C-c O" 'multi-occur)
(bind-key "C-c C-o" 'multi-occur-in-matching-buffers)

;; View occurrence in occur mode
(bind-key "v" 'occur-mode-display-occurrence occur-mode-map)
(bind-key "n" 'next-line occur-mode-map)
(bind-key "p" 'previous-line occur-mode-map)

;; ;; I don't need to kill emacs that easily
;; the mnemonic is C-x REALLY QUIT
;; (bind-key "C-x r q" 'save-buffers-kill-terminal)
;; (bind-key "C-x C-c" 'delete-frame)



(require 'init-keychord-bindings)
(require 'init-hydra-bindings)
(require 'init-editing-bindings)

(when seartipy-super-key
  (require 'init-key-super))

(provide 'init-keybindings)
