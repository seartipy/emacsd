(use-package launchctl
  :if *is-a-mac*
  :defer t
  :init
  (progn
    (add-to-list 'auto-mode-alist '("\\.plist\\'" . nxml-mode))
    (evil-leader/set-key "al" 'launchctl)))

(use-package osx-dictionary
  :if *is-a-mac*
  :init (evil-leader/set-key "xwd" 'osx-dictionary-search-pointer)
  :commands (osx-dictionary-search-pointer
             osx-dictionary-search-input
             osx-dictionary-cli-find-or-recompile))

(use-package osx-trash
  :if (and *is-a-mac*
           (not (boundp 'mac-system-move-file-to-trash-use-finder)))
  :init (osx-trash-setup))

(use-package pbcopy
  :if (and *is-a-mac* (not (display-graphic-p)))
  :init (turn-on-pbcopy))

(use-package reveal-in-osx-finder
  :if *is-a-mac*
  :commands reveal-in-osx-finder)

(use-package ahk-mode
  :mode "\\.ahk\\'"
  :if *is-a-windows*
  :defer t

  :init
  (evil-leader/set-key-for-mode ahk-mode
    "cb" 'ahk-comment-block-dwim
    "cc" 'ahk-comment-dwim
    "eb" 'ahk-run-script
    "hh" 'ahk-lookup-web
    "hH" 'ahk-lookup-chm))

(use-package system-packages :defer t)
(use-package symon :defer t)

(provide 'init-os)
