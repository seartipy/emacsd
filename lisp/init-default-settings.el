(setq custom-file (expand-file-name ".custom.el" user-emacs-directory))

(when (file-exists-p custom-file)
  (load custom-file))



(defconst *is-a-mac* (eq system-type 'darwin))
(defconst *is-a-linux* (eq system-type 'gnu/linux))
(defconst *is-a-windows* (eq system-type 'windows-nt))



(when *is-a-mac*
  (setq mac-option-modifier 'meta)
  (setq ns-function-modifier 'hyper)) ; make Fn key do Hyper

(when *is-a-windows*
  (setq inhibit-compacting-font-caches t)

  (setq w32-pass-lwindow-to-system nil)
  (setq w32-lwindow-modifier 'super)

  (setq w32-pass-rwindow-to-system nil)
  (setq w32-rwindow-modifier 'super)

  (setq w32-pass-apps-to-system nil)
  (setq w32-apps-modifier 'hyper))

(add-hook 'emacs-startup-hook
          (lambda ()
            (when *is-a-windows*
              (setq default-directory "~/"))))

(when (fboundp 'xterm-mouse-mode)
  (xterm-mouse-mode t))



;; Handier way to add modes to auto-mode-alist
(defun add-auto-mode (mode &rest patterns)
  "Add entries to `auto-mode-alist' to use `MODE' for all given file `PATTERNS'."
  (dolist (pattern patterns)
    (add-to-list 'auto-mode-alist (cons pattern mode))))



(setq-default blink-cursor-interval 0.4
              buffers-menu-max-size 30
              case-fold-search t
              column-number-mode t
              delete-selection-mode t
              indent-tabs-mode nil
              make-backup-files nil
              mouse-yank-at-point nil
              save-interprogram-paste-before-kill t
              scroll-preserve-screen-position 'always
              set-mark-command-repeat-pop t
              show-trailing-whitespace t
              tooltip-delay 1.5
              truncate-lines nil
              x-gtk-use-system-tooltips nil
              truncate-partial-width-windows nil
              visible-bell nil)

(setq use-file-dialog nil
      use-dialog-box nil
      inhibit-startup-screen t
      inhibit-startup-echo-area-message t
      indicate-empty-lines t)

;; do not ask follow link
(customize-set-variable 'find-file-visit-truename t)

(let ((no-border '(internal-border-width . 0)))
  (add-to-list 'default-frame-alist no-border)
  (add-to-list 'initial-frame-alist no-border))

(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'set-scroll-bar-mode)
  (set-scroll-bar-mode nil))
(when (fboundp 'menu-bar-mode)
  (menu-bar-mode -1))

(global-auto-revert-mode)
(setq global-auto-revert-non-file-buffers t
      auto-revert-verbose nil)

(transient-mark-mode t)

(global-prettify-symbols-mode)

(show-paren-mode)

;; Don't disable case-change functions
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(cua-selection-mode t)

(electric-indent-mode t)

(setq create-lockfiles nil)

;; (setq auto-save-default nil)

(fset 'yes-or-no-p 'y-or-n-p)

(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

;; Non-zero values for `line-spacing' can mess up ansi-term and co,
;; so we zero it explicitly in those cases.
(add-hook 'term-mode-hook
          (lambda ()
            (setq line-spacing 0)))

;; Don't disable narrowing commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

(setq hippie-expand-try-functions-list
      '(try-complete-file-name-partially
        try-complete-file-name
        try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill))

;; fix slow tramp
(setq tramp-ssh-controlmaster-options
      "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=no")

(provide 'init-default-settings)
