;;;; keys

(use-package bind-map
  :config
  (bind-map seartipy-mode-map
    :keys ((concat "M-" seartipy-leader-key) seartipy-emacs-leader-key)
    :override-minor-modes t
    :evil-keys (seartipy-leader-key)
    :evil-states (normal visual motion)))

(defmacro seartipy/set-key (&rest bindings)
  `(bind-map-set-keys seartipy-mode-map ,@bindings))
(put 'seartipy/set-key 'lisp-indent-function 'defun)
(defalias 'evil-leader/set-key 'seartipy/set-key)

(defun seartipy/init-leader-map (mode keymap leaders emacs-leaders)
  (or (boundp keymap)
      (progn
        (eval
         `(bind-map ,keymap
            :major-modes (,mode)
            :keys ,emacs-leaders
            :evil-keys ,leaders
            :evil-states (normal motion visual)))
        (boundp keymap))))

(defmacro seartipy/set-key-for-mode (mode &rest bindings)
  (let ((keymap (intern (format "%s-bs-map" mode)))
        (emacs-leaders '((concat "M-" seartipy-major-mode-leader-key)
                         (concat "M-" seartipy-leader-key " m")
                         (concat seartipy-emacs-leader-key " m")
                         seartipy-major-mode-emacs-leader-key))
        (leaders '(seartipy-major-mode-leader-key
                   (concat seartipy-leader-key " m"))))
    (seartipy/init-leader-map mode keymap leaders emacs-leaders)
    `(bind-map-set-keys ,keymap ,@bindings)))
(put 'seartipy/set-key-for-mode 'lisp-indent-function 'defun)
(defalias 'evil-leader/set-key-for-mode 'seartipy/set-key-for-mode)



(use-package key-chord
  :config
  (key-chord-mode +1))

(use-package use-package-chords)

(use-package hydra
  :defer t

  :config
  (require 'hydra-examples))



;;;; which-key

(use-package which-key
  :diminish which-key-mode

  :init
  (evil-leader/set-key "hk" 'which-key-show-top-level)
  :config
  (defun seartipy/declare-prefix (prefix description)
    (which-key-declare-prefixes
      (concat seartipy-leader-key " " prefix) description
      (concat "M-" seartipy-leader-key " " prefix) description
      (concat seartipy-emacs-leader-key " " prefix) description))

  (defun seartipy/declare-prefix-for-mode (mode prefix description)
    (which-key-declare-prefixes-for-mode mode
      (concat seartipy-leader-key " m" prefix) description
      (concat "M-" seartipy-leader-key " m" prefix) description
      (concat seartipy-emacs-leader-key " m" prefix) description ;; not working
      (concat seartipy-major-mode-leader-key " " prefix) description
      (concat "M-" seartipy-major-mode-leader-key " " prefix) description
      (concat seartipy-major-mode-emacs-leader-key " " prefix) description))

  (defun seartipy/declare-prefixes (key desc &rest bindings)
    (while key
      (seartipy/declare-prefix key desc)
      (setq key (pop bindings)
            desc (pop bindings))))
  (defun seartipy/declare-prefixes-for-mode (mode key desc &rest bindings)
    (while key
      (seartipy/declare-prefix-for-mode mode key desc)
      (setq key (pop bindings)
            desc (pop bindings))))

  (put 'seartipy/declare-prefix 'lisp-indent-function 'defun)
  (put 'seartipy/declare-prefixes 'lisp-indent-function 'defun)
  (put 'seartipy/declare-prefix-for-mode 'lisp-indent-function 'defun)
  (put 'seartipy/declare-prefixes-for-mode 'lisp-indent-function 'defun)

  (seartipy/declare-prefixes
    "a"   "applications"
    "b"   "buffers"
    "bm"  "move"
    "c"   "compile/comments"
    "C"   "capture/colors"
    "e"   "errors"
    "f"   "files"
    "fC"  "files/convert"
    "g"   "git/versions-control"
    "h"   "helm/help/highlight"
    "hd"  "help-describe"
    "i"   "insertion"
    "il"  "lorem"
    ;; "j"   "join/split"
    ;; "k"   "lisp"
    ;; "kd"  "delete"
    ;; "kD"  "delete-backward"
    ;; "k`"  "hybrid"
    "n"   "narrow/numbers"
    "o" "other"
    "p"   "projects"
    "p$"  "projects/shell"
    "q"   "quit"
    "r"   "registers/rings"
    "Re"  "elisp"
    "Rp"  "pcre"
    "s"   "search/symbol"
    "sa"  "ag"
    "sg"  "grep"
    "S"   "spell"
    "t"   "toggles"
    "tC"  "colors"
    "th"  "highlight"
    "w"   "windows"
    ;; "wp"  "popup"
    "x"   "text"
    "xa"  "align"
    "xd"  "delete"
    ;; "xl"  "lines"
    ;; "xm"  "move"
    "xt"  "transpose"
    ;; "xw"  "words"
    ;;"z"   "zoom"
    )

  (which-key-mode))



(if (member seartipy-default-editing-style '(vim emacs hybrid))
    (when (not (eq seartipy-default-editing-style 'emacs)) ;; just an optimization
      (require 'init-evil))

  (warn (format "%s no such editing style" seartipy-default-editing-style)))



(use-package linum-relative
  :diminish linum-relative-mode

  :init
  (evil-leader/set-key "tr" 'linum-relative-toggle)

  :config
  (add-hook 'after-init-hook 'linum-relative-on))

(use-package discover-my-major :defer t)

(provide 'init-keys)
