;;;; web

(use-package json-mode
  :mode "\\.json\\'")

(use-package typescript-mode
  :defer t
  :init
  (customize-set-variable 'typescript-indent-level 2))

(use-package add-node-modules-path :defer t)

(defun seartipy/setup-tide-mode ()
  (interactive)
  (when (or (locate-dominating-file default-directory "tsconfig.json")
            (locate-dominating-file default-directory "jsconfig.json"))
    (tide-setup)
    (tide-hl-identifier-mode)
    (eldoc-mode +1)
    (add-node-modules-path)
    (company-mode +1)))

(defun seartipy/use-eslint-from-node-modules ()
  (let* ((root (locate-dominating-file default-directory "package.json"))
         (package-directory (expand-file-name "node_modules/eslint" root)))
    (when (file-directory-p package-directory)
      (setq flycheck-javascript-eslint-executable
            (expand-file-name "bin/eslint.js" package-directory)))))

(use-package web-mode
  :mode ("\\.jsx\\'" "\\.tsx\\'")

  :init
  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(javascript-jshint)))

  (with-eval-after-load 'flycheck
    (flycheck-add-mode 'javascript-eslint 'web-mode)
    ;; (flycheck-add-next-checker 'javascript-eslint 'javascript-tide 'append)
    ;; (flycheck-add-next-checker 'javascript-eslint 'jsx-tide 'append)
    (flycheck-add-mode 'typescript-tslint 'web-mode))

  (defun my-web-mode-hook ()
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-code-indent-offset 2)

    (emmet-mode)
    (company-mode +1)
    (eldoc-mode +1)
    (seartipy/use-eslint-from-node-modules)
    (seartipy/setup-tide-mode))

  (add-hook 'web-mode-hook  'my-web-mode-hook))

(use-package js2-mode
  :mode "\\.js\\'"

  :init
  (seartipy/declare-prefix-for-mode 'js2-mode "z" "folding")

  (evil-leader/set-key-for-mode js2-mode
    "w" 'js2-mode-toggle-warnings-and-errors
    "zc" 'js2-mode-hide-element
    "zo" 'js2-mode-show-element
    "zr" 'js2-mode-show-all
    "ze" 'js2-mode-toggle-element
    "zF" 'js2-mode-toggle-hide-functions
    "zC" 'js2-mode-toggle-hide-comments)

  ;; Disable js2 js2-mode's syntax error highlighting by default...
  (setq-default js2-mode-show-parse-errors nil
                js2-mode-show-strict-warnings nil
                js2-strict-missing-semi-warning nil
                js2-basic-offset 2
                js-indent-level 2)

  (add-hook 'js2-mode-hook
            (lambda ()
              (setq mode-name "JS")
              (js2-imenu-extras-setup)
              (js2-mode-hide-warnings-and-errors)
              (company-mode)
              (seartipy/use-eslint-from-node-modules)
              (rainbow-delimiters-mode))))

(defun enable-minor-mode (my-pair)
  "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
  (if (buffer-file-name)
      (if (string-match (car my-pair) buffer-file-name)
          (funcall (cdr my-pair)))))

(use-package prettier-js
  :diminish prettier-js-mode
  :defer t

  :init
  (setq prettier-js-args '("--single-quote" "true"))
  (add-hook 'js2-mode-hook 'prettier-js-mode)
  (add-hook 'typescript-mode-hook 'prettier-js-mode)
  (add-hook 'web-mode-hook #'(lambda ()
                               (enable-minor-mode
                                '("\\.tsx?\\'" . prettier-js-mode))
                               (enable-minor-mode
                                '("\\.jsx?\\'" . prettier-js-mode)))))

(use-package tide
  :diminish tide-mode
  :defer t
  :init
  (setq tide-tsserver-executable "node_modules/typescript/bin/tsserver")
  ;; aligns annotation to the right hand side
  ;; (setq company-tooltip-align-annotations t)

  ;; formats the buffer before saving
  ;; (add-hook 'before-save-hook 'tide-format-before-save)

  (add-hook 'typescript-mode-hook (lambda ()
                                    (setq mode-name "TS")
                                    (seartipy/setup-tide-mode)))

  (add-hook 'js2-mode-hook 'seartipy/setup-tide-mode)
  ;; format options
  ;; (setq tide-format-options '(:insertSpaceAfterFunctionKeywordForAnonymousFunctions t :placeOpenBraceOnNewLineForFunctions nil))

  :config
  (flycheck-add-next-checker 'jsx-tide '(warning . javascript-eslint) 'append)
  (flycheck-add-next-checker 'tsx-tide '(warning . javascript-eslint) 'append)

  (seartipy/declare-prefixes-for-mode 'typescript-mode
    "g" "goto"
    "h" "help"
    "r" "refactor"
    "e" "errors"
    "S" "server")

  (evil-leader/set-key-for-mode typescript-mode
    "gb" 'tide-jump-back
    "gu" 'tide-references
    "rR" 'tide-refactor
    "rr" 'tide-rename-symbol
    "ef" 'tide-fix
    "hh" 'tide-documentation-at-point
    "ee" 'tide-project-errors
    "Sr" 'tide-restart-server))

(use-package nodejs-repl
  :bind (:map
         js2-mode-map
         ("C-x C-e" . nodejs-repl-send-last-sexp)
         ("C-c C-r" . nodejs-repl-send-region)
         ("C-c C-l" . nodejs-repl-load-file)
         ("C-c C-z" . nodejs-repl-switch-to-repl))
  :init
  (if (executable-find "babel-node")
      (setq nodejs-repl-command "babel-node"))

  (seartipy/declare-prefixes-for-mode 'js2-mode
    "n" "nodejs"
    "s" "nodejs repl")
  (evil-leader/set-key-for-mode js2-mode
    "ns" 'nodejs-repl
    "se" 'nodejs-repl-send-last-sexp
    "sr" 'nodejs-repl-send-region
    "sa" 'nodejs-repl-load-file
    "sb" 'nodejs-repl-send-buffer
    "sq" 'nodejs-repl-quit-or-cancel
    "si" 'nodejs-repl-switch-to-repl))


(use-package livid-mode :defer t)

(use-package skewer-mode
  :defer t
  :init
  (defun seartipy/skewer-start-repl ()
    "Attach a browser to Emacs and start a skewer REPL."
    (interactive)
    (run-skewer)
    (skewer-repl))
  (defun seartipy/skewer-eval-region (beg end)
    "Execute the region as JavaScript code in the attached browser."
    (interactive "r")
    (skewer-eval (buffer-substring beg end) #'skewer-post-minibuffer))


  (seartipy/declare-prefixes-for-mode 'js2-mode
    "b"   "skewer"
    "be" "eval"
    "bs" "repl")

  (evil-leader/set-key-for-mode js2-mode
    "'"   'run-skewer
    "bee" 'skewer-eval-last-expression
    "beE" 'skewer-eval-print-last-expression
    "bsb" 'skewer-load-buffer
    "bsi" 'seartipy/skewer-start-repl
    "bsf" 'skewer-eval-defun
    "bsr" 'seartipy/skewer-eval-region
    "bss" 'skewer-repl))

;; refactoring support for javascript
(use-package js2-refactor
  :diminish js2-refactor-mode
  :defer t

  :init
  (add-hook 'js2-mode-hook (lambda ()
                             (require 'js2-refactor)
                             (js2-refactor-mode)))
  (seartipy/declare-prefixes-for-mode 'js2-mode
    "r" "refactor"
    "r3" "ternary"
    "ra" "add/args"
    "rb" "barf"
    "rc" "contract"
    "re" "expand/extract"
    "ri" "inline/inject/introduct"
    "rl" "localize/log"
    "rr" "rename"
    "rs" "split/slurp"
    "rt" "toggle"
    "ru" "unwrap"
    "rv" "var"
    "rw" "wrap"
    "x" "text")

  (evil-leader/set-key-for-mode js2-mode
    "r3i" 'js2r-ternary-to-if
    "rag" 'js2r-add-to-globals-annotation
    "rao" 'js2r-arguments-to-object
    "rba" 'js2r-forward-barf
    "rca" 'js2r-contract-array
    "rco" 'js2r-contract-object
    "rcu" 'js2r-contract-function
    "rea" 'js2r-expand-array
    "ref" 'js2r-extract-function
    "rem" 'js2r-extract-method
    "reo" 'js2r-expand-object
    "reu" 'js2r-expand-function
    "rev" 'js2r-extract-var
    "rig" 'js2r-inject-global-in-iife
    "rip" 'js2r-introduce-parameter
    "riv" 'js2r-inline-var
    "rlp" 'js2r-localize-parameter
    "rlt" 'js2r-log-this
    "rrv" 'js2r-rename-var
    "rsl" 'js2r-forward-slurp
    "rss" 'js2r-split-string
    "rsv" 'js2r-split-var-declaration
    "rtf" 'js2r-toggle-function-expression-and-declaration
    "ruw" 'js2r-unwrap
    "rvt" 'js2r-var-to-this
    "rwi" 'js2r-wrap-buffer-in-iife
    "rwl" 'js2r-wrap-in-for-loop
    "k" 'js2r-kill
    "xmj" 'js2r-move-line-down
    "xmk" 'js2r-move-line-up)

  :config
  (js2r-add-keybindings-with-prefix "C-c C-m"))

;;emmet-mode setup for emmet support in html/css files
(use-package emmet-mode
  :defer t
  :diminish emmet-mode

  :init
  (add-hook 'sgml-mode-hook 'emmet-mode)
  (add-hook 'css-mode-hook  'emmet-mode)
  (add-hook 'emmet-mode-hook (lambda () (setq emmet-indentation 4)))
  (setq emmet-move-cursor-between-quotes t))

(use-package json-snatcher
  :defer t

  :init
  (evil-leader/set-key-for-mode json-mode
    "hp" 'jsons-print-path))

(use-package restclient
  :mode ("\\.http\\'" . restclient-mode)
  :init
  (evil-leader/set-key-for-mode restclient-mode
    "n" 'restclient-jump-next
    "p" 'restclient-jump-prev
    "s" 'restclient-http-send-current-stay-in-window
    "S" 'restclient-http-send-current
    "r" 'restclient-http-send-current-raw
    "y" 'restclient-copy-curl-command)

  (add-hook 'restclient-mode-hook 'company-mode)

  :config
  (use-package company-restclient
    :defer t
    :init
    (with-eval-after-load 'company
      (add-to-list 'company-backends 'company-restclient)))

  (use-package restclient-helm
    :init
    (evil-leader/set-key-for-mode restclient-mode
      "ji" 'helm-restclient)))

(provide 'init-web)
