;;;; cpp

(use-package irony
  :diminish irony-mode
  :defer t
  :init
  (custom-set-variables '(irony-additional-clang-options '("-std=c++1z")))
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)

  ;; replace the `completion-at-point' and `complete-symbol' bindings in
  ;; irony-mode's buffers by irony-mode's function
  (defun seartipy/irony-mode-hook ()
    (company-mode)
    (eldoc-mode)
    (bind-key [remap completion-at-point]
              'irony-completion-at-point-async
              irony-mode-map)
    (bind-key [remap complete-symbol]
              'irony-completion-at-point-async
              irony-mode-map))
  (add-hook 'irony-mode-hook 'seartipy/irony-mode-hook)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

(use-package flycheck-irony
  :defer t
  :init
  (with-eval-after-load 'flycheck
    (add-hook 'flycheck-mode-hook #'flycheck-irony-setup)))

(use-package company-irony
  :defer t
  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-irony)))

(use-package irony-eldoc
  :defer t
  :init
  (add-hook 'irony-mode-hook 'irony-eldoc))

(use-package clang-format
  :defer t)

(use-package disaster
  :defer t
  :commands (disaster)

  :init
  (evil-leader/set-key-for-mode c-mode
    "D" 'disaster)
  (evil-leader/set-key-for-mode c++-mode
    "D" 'disaster))

(use-package cmake-mode
  :mode (("CMakeLists\\.txt\\'" . cmake-mode) ("\\.cmake\\'" . cmake-mode))

  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-cmake)))

(use-package company-c-headers
  :defer t

  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-c-headers)))

(use-package gdb-mi
  :defer t

  :init
  (setq
   ;; use gdb-many-windows by default when `M-x gdb'
   gdb-many-windows t
   ;; Non-nil means display source file containing the main routine at startup
   gdb-show-main t))

(provide 'init-cpp)
