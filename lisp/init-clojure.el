;;;; clojure

(use-package clojure-mode
  :defer t

  :init
  ;; (setq clojure-align-forms-automatically t)
  (defun clojure-mode-setup ()
    "Enable features useful in any Lisp mode."
    (subword-mode)
    (company-mode)
    (rainbow-delimiters-mode)
    (aggressive-indent-mode)
    (lispy-mode)
    (smartparens-strict-mode)
    (helm-cider-mode)
    (eval-sexp-fu-flash-mode)
    (add-hook 'after-save-hook 'check-parens nil t))

  (with-eval-after-load 'clojure-mode
    (add-hook 'clojure-mode-hook 'clojure-mode-setup))

  :config
  (define-clojure-indent
    (defroutes 'defun)
    (GET 2)
    (POST 2)
    (PUT 2)
    (DELETE 2)
    (HEAD 2)
    (ANY 2)
    (context 2)))

(use-package cider-eval-sexp-fu
  :defer t

  :init
  (with-eval-after-load 'eval-sexp-fu
    (require 'cider-eval-sexp-fu)))

(use-package inf-clojure
  :defer t

  :init
  (add-hook 'inf-clojure-mode-hook #'eldoc-mode))

(use-package flycheck-clojure :defer t)

(use-package 4clojure :defer t )
(use-package clojure-cheatsheet :defer t)
(use-package helm-clojuredocs :defer t)
(use-package helm-cider :defer t)

(defun cider-figwheel-repl ()
  (interactive)
  (save-some-buffers)
  (with-current-buffer (cider-current-repl-buffer)
    (goto-char (point-max))
    (insert "(require 'figwheel-sidecar.repl-api)
             (figwheel-sidecar.repl-api/start-figwheel!) ; idempotent
             (figwheel-sidecar.repl-api/cljs-repl)")
    (cider-repl-return)))

(bind-key "C-c M-f" #'cider-figwheel-repl)

(use-package cider
  :init
  (setq nrepl-popup-stacktraces nil)
  (setq cider-show-error-buffer nil)
  (setq cider-repl-use-clojure-font-lock t)

  (setq cider-overlays-use-font-lock t)
  (setq nrepl-buffer-name-show-port t)
  (setq cider-prompt-save-file-on-load nil)

  (setq cider-repl-wrap-history t)
  (setq cider-repl-pop-to-buffer-on-connect nil)

  (setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")

  (defun cider-repl-mode-setup ()
    (subword-mode)
    (company-mode)
    (rainbow-delimiters-mode)
    (helm-cider-mode)
    (lispy-mode)
    (smartparens-strict-mode)
    (setq show-trailing-whitespace nil))

  (add-hook 'cider-repl-mode-hook 'cider-repl-mode-setup)
  (add-hook 'cider-mode-hook 'eldoc-mode)
  (seartipy/declare-prefixes-for-mode 'clojure-mode
    "h" "doc"
    "e" "eval"
    "f" "format"
    "g" "goto"
    "s" "cider"
    "t" "tests"
    "T" "more tests"
    "r" "refactor"
    "d" "debug")

  (dolist (mode '(clojure-mode clojurec-mode clojurex-mode clojurescript-mode))
    (evil-leader/set-key-for-mode clojure-mode
      "ha" 'cider-apropos
      "hh" 'cider-doc
      "hg" 'cider-grimoire
      "hj" 'cider-javadoc
      "hn" 'cider-browse-ns

      "eb" 'cider-eval-buffer
      "ee" 'cider-eval-last-sexp
      "ef" 'cider-eval-defun-at-point
      "em" 'cider-macroexpand-1
      "eM" 'cider-macroexpand-all
      "er" 'cider-eval-region
      "ep" 'cider-eval-sexp-at-point
      "ew" 'cider-eval-last-sexp-and-replace
      "en" 'cider-eval-ns-form

      "="  'cider-format-buffer
      "fb" 'cider-format-buffer

      "gb" 'cider-pop-back
      "gC" 'cider-classpath
      "ge" 'cider-jump-to-compilation-error
      "gr" 'cider-jump-to-resource
      "gn" 'cider-browse-ns
      "gN" 'cider-browse-ns-all
      "gg" 'cider-find-var
      "gr" 'cider-jump-to-resource

      "'"  'cider-jack-in
      "\""  'cider-jack-in-clojurescript
      "sb" 'cider-load-buffer

      "sc" 'cider-connect
      "sC" 'cider-find-and-clear-repl-output
      "so" 'cider-repl-switch-to-other

      "si" 'cider-jack-in
      "sI" 'cider-jack-in-clojurescript

      "sq" 'cider-quit

      "ss" 'cider-switch-to-repl-buffer
      "sx" 'cider-refresh

      "tb" 'cider-test-show-report
      "tt" 'cider-test-run-test
      "tn" 'cider-test-run-ns-tests
      "tl" 'cider-test-run-loaded-tests
      "tp" 'cider-test-run-project-tests
      "tr" 'cider-test-rerun-tests
      "ts" 'cider-test-show-report

      "Te" 'cider-enlighten-mode
      "Tt" 'cider-auto-test-mode

      "se" 'cider-insert-last-sexp-in-repl
      "sE" 'cider-insert-last-sexp-in-repl-focus
      "sf" 'cider-insert-last-defun-in-repl
      "sF" 'cider-insert-last-defun-in-repl-focus
      "sn" 'cider-insert-ns-form-to-repl
      "sN" 'cider-insert-ns-form-to-repl-focus
      "sr" 'cider-insert-region-to-repl
      "sR" 'cider-insert-region-to-repl-focus

      "db" 'cider-debug-defun-at-point
      "dv" 'cider-inspect

      "rc{" 'clojure-convert-collection-to-map
      "rc(" 'clojure-convert-collection-to-list
      "rc'" 'clojure-convert-collection-to-quoted-list
      "rc#" 'clojure-convert-collection-to-set
      "rc[" 'clojure-convert-collection-to-vector))

  (evil-leader/set-key-for-mode cider-repl-mode
    "hh" 'cider-doc
    "hg" 'cider-grimoire
    "hj" 'cider-javadoc

    "ee" 'cider-eval-last-sexp
    "ef" 'cider-eval-defun-at-point
    "er" 'cider-eval-region
    "ew" 'cider-eval-last-sexp-and-replace

    "gb" 'cider-jump-back
    "ge" 'cider-jump-to-compilation-error
    "gg" 'cider-find-var
    "gr" 'cider-jump-to-resource

    "sc" 'cider-repl-clear-buffer
    "sn" 'cider-repl-set-ns
    "sq" 'cider-quit
    "ss" 'cider-switch-to-last-clojure-buffer
    "sx" 'cider-refresh
    "di" 'cider-inspect)

  :config
  (flycheck-clojure-setup)
  (bind-key "C-c C-d" 'ac-cider-popup-doc cider-mode-map))

(use-package clj-refactor
  :defer t
  :diminish clj-refactor-mode

  :init
  (add-hook 'clojure-mode-hook 'clj-refactor-mode)

  :config
  (cljr-add-keybindings-with-prefix "C-c C-f")
  (seartipy/declare-prefix-for-mode 'clojure-mode
    "x" "cljr-hydra")
  (evil-leader/set-key-for-mode clojure-mode
    "x" (lookup-key clj-refactor-map (kbd "C-c C-f h"))))

;; (use-package yesql-ghosts
;;   :defer t
;;   :init
;;   (add-hook 'cider-mode-hook 'yesql-ghosts-auto-show-ghosts))

(provide 'init-clojure)
