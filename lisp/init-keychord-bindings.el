;;;; keychords

(key-chord-define-global "\\\\" 'avy-goto-word-or-subword-1)
(key-chord-define-global "qq" 'avy-goto-char-2)
(key-chord-define-global "QQ" 'avy-goto-word-1)
(key-chord-define-global "zz" 'zap-up-to-char)
(key-chord-define-global "qs" 'cycle-spacing)


(key-chord-define-global "jj" 'avy-goto-word-1)
(key-chord-define-global "jl" 'avy-goto-line)
(key-chord-define-global "jk" 'avy-goto-char)

;; (key-chord-define-global "JJ" 'crux-switch-to-previous-buffer)
;; (key-chord-define-global "uu" 'undo-tree-visualize)
;; (key-chord-define-global "xx" 'execute-extended-command)
;; (key-chord-define-global "yy" 'browse-kill-ring)

(provide 'init-keychord-bindings)
