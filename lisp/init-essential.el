(use-package no-littering
  :config
  (setq auto-save-file-name-transforms
        `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))

(use-package seq)
(use-package dash)
(use-package fullframe :defer t)



(use-package server
  :config
  (unless (server-running-p)
    (server-start)))



(use-package uniquify
  :defer t
  :ensure nil
  :init
  (setq uniquify-buffer-name-style 'reverse
        uniquify-separator " • "
        uniquify-after-kill-buffer-p t
        uniquify-ignore-buffers-re "^\\*"))



(use-package ibuffer
  :defer t
  :init
  (setq ibuffer-filter-group-name-face 'font-lock-doc-face)
  (setq-default ibuffer-show-empty-filter-groups nil)

  :config
  (fullframe ibuffer ibuffer-quit))



(use-package dired
  :defer t
  :ensure nil
  :init
  (setq-default diredp-hide-details-initially-flag nil
                dired-dwim-target t)
  (evil-leader/set-key
    "jd" 'dired-jump
    "jD" 'dired-jump-other-window)

  :config
  (when (fboundp 'global-dired-hide-details-mode)
    (global-dired-hide-details-mode -1))
  (setq dired-recursive-deletes 'top)
  (bind-key [mouse-2] 'dired-find-file dired-mode-map))

(use-package dired-x
  :ensure nil
  :defer t)



(use-package anzu
  :diminish anzu-mode

  :config
  (global-anzu-mode))

(use-package restart-emacs
  :defer t

  :init
  (defun reboot-emacs (&optional args)
    "Restart emacs."
    (interactive)
    (restart-emacs args))

  (defun reboot-emacs-resume-layouts (&optional args)
    "Restart emacs and resume layouts."
    (interactive)
    (restart-emacs (cons "--resume-layouts" args)))

  (defun reboot-emacs-debug-init (&optional args)
    "Restart emacs and enable debug-init."
    (interactive)
    (restart-emacs (cons "--debug-init" args)))

  (evil-leader/set-key
    "qq" 'save-buffers-kill-terminal
    "qQ" 'kill-emacs
    "qd" 'reboot-emacs-debug-init
    "qr" 'reboot-emacs-resume-layouts
    "qR" 'reboot-emacs))

(use-package noflet
  :config
  ;; do not ask to kill processes
  (defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
    (noflet ((process-list ())) ad-do-it)))

(provide 'init-essential)
