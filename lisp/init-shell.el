(use-package multi-term
  :if (not *is-a-windows*)
  :bind (("<f5>" . multi-term-dedicated-toggle))

  :init
  (custom-set-variables '(multi-term-dedicated-select-after-open-p t))
  (setq multi-term-scroll-to-bottom-on-output "others")
  (setq multi-term-scroll-show-maximum-output +1)

  (evil-leader/set-key-for-mode term-mode
    "c" 'multi-term
    "p" 'multi-term-prev
    "n" 'multi-term-next)

  (defun seartipy/projectile-multi-term-in-root ()
    "Invoke `multi-term' in the project's root."
    (interactive)
    (projectile-with-default-dir (projectile-project-root) (multi-term)))

  (evil-leader/set-key
    "p'" 'projectile-run-shell
    "p$t" 'seartipy/projectile-multi-term-in-root)

  :config
  (add-to-list 'term-bind-key-alist '("M-DEL" . term-send-backward-kill-word))
  (add-to-list 'term-bind-key-alist '("M-d" . term-send-forward-kill-word)))


(use-package eshell
  :defer t
  :init
  (setq eshell-cmpl-cycle-completions nil
        eshell-buffer-maximum-lines 20000
        eshell-history-size 350
        eshell-hist-ignoredups t
        ;; buffer shorthand -> echo foo > #'buffer
        eshell-buffer-shorthand t
        eshell-highlight-prompt t
        eshell-plain-echo-behavior t)

  (autoload 'eshell-delchar-or-maybe-eof "em-rebind")

  :config
  (require 'esh-opt)

  (defalias 'eshell/e 'find-file-other-window)
  (defalias 'eshell/d 'dired)
  (setenv "PAGER" "cat")

  (require 'em-smart)
  (setq eshell-where-to-jump 'begin
        eshell-review-quick-commands nil
        eshell-smart-space-goes-to-end t)
  (add-hook 'eshell-mode-hook 'eshell-smart-initialize)

  ;; Visual commands
  (require 'em-term)
  (mapc (lambda (x) (push x eshell-visual-commands))
        '("el" "elinks" "htop" "less" "ssh" "tmux" "top"))

  (add-hook 'eshell-mode-hook 'company-mode)

  (add-hook 'eshell-mode-hook
            (lambda ()
              (setq-local company-idle-delay 0.5)
              ;; The default frontend screws everything up in short windows like
              ;; terminal often are
              (setq-local company-frontends '(company-preview-frontend))))

  ;; These don't work well in normal state
  ;; due to evil/emacs cursor incompatibility
  (with-eval-after-load 'evil
    (evil-define-key 'insert eshell-mode-map
                     (kbd "C-k") 'eshell-previous-matching-input-from-input
                     (kbd "C-j") 'eshell-next-matching-input-from-input)))

(use-package esh-help
  :defer t
  :init
  (add-hook 'eshell-mode-hook 'eldoc-mode)
  :config
  (setup-esh-help-eldoc))

(setq comint-prompt-read-only t)

(use-package eshell-z
  :defer t

  :init
  (with-eval-after-load 'eshell
    (require 'eshell-z)))

(use-package shell-pop
  :defer t
  :init
  (custom-set-variables
   '(shell-pop-full-span t)
   '(shell-pop-window-size 20)
   '(shell-pop-shell-type '("eshell" "*eshell*" (lambda () (eshell shell-pop-term-shell))))
   '(shell-pop-universal-key "<f7>"))

  (when *is-a-windows*
    (bind-key "<f5>" 'shell-pop))

  (add-hook 'term-mode-hook
            (lambda ()
              (setq-local global-hl-line-mode nil)
              (linum-mode -1))))

(use-package eshell-git-prompt
  :defer t
  :config
  (eshell-git-prompt-use-theme 'powerline))

(provide 'init-shell)
