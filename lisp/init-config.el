(defconst seartipy-supported-layers
  '(ui
    shell
    term
    tools
    os
    themes
    web
    clojure
    scala
    python
    cpp
    haskell
    markdown
    ))

(defvar seartipy-layers '(ui term)
  "Select the layers you want enabled from
`clojure'
`scala'
`python'
`haskell'
`web'
`cpp'
`shell'
`elisp'
`ui'
`term'
`tools'
`themes'
`os'
`markdown'
")

(defvar seartipy-theme 'material
  "Select one of the themes from
`material',
`material-light',
`spacemacs-dark',
`spacemacs-light'
`solarized-dark',
`solarized-light'
`dracula'
`monokai'
`darkokai'
Solarized does not work well in terminal.")

(defvar seartipy-leader-key "SPC"
  "The leader key. Use `M-<leader>' in `emacs state' and `insert state'")

(defvar seartipy-emacs-leader-key "M-m"
  "The leader key accessible in `emacs state' and `insert state'")

(defvar seartipy-major-mode-leader-key "RET"
  "Major mode leader key is a shortcut key which is the equivalent of
pressing `<leader> m`. Use `M-<leader>' in `emacs state' and `insert state'")

(defvar seartipy-major-mode-emacs-leader-key "C-M-m"
  "Major mode leader key accessible in `emacs state' and `insert state'")

(defvar  seartipy-default-editing-style 'emacs
  "One of `vim', `emacs' or `hybrid' . `hybrid' allows you to use all `emacs' commands in `insert' mode.")

(defvar seartipy-fonts
  '("Consolas"
    "Monaco"
    "Ubuntu Mono"
    "Roboto Mono"
    "mononoki"
    "Inconsolata"
    "Source Code Pro"
    "Fira Code"
    "Hack"
    "Dejavu Sans Mono")
  "The first available font, will be set")

(defvar seartipy-font-size 13
  "default font size")

(defvar seartipy-line-numbers nil
  "Set it to `t' if you want to enable line numbers by default")

(defvar seartipy-lispy nil
  "If not nil,lispy is enabled")

(defvar seartipy-super-key nil
  "If not nil, super keybindings will be enabled")

(defvar seartipy-dashboard nil
  "If not nil, spacemacs like dashboard is shown at emacs start")

(provide 'init-config)
