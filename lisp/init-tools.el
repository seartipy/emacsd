(use-package docker
  :defer t

  :init
  (seartipy/declare-prefixes "D" "Docker")

  (evil-leader/set-key
    "Dc" 'docker-containers
    "Dd" 'docker-rmi
    "De" 'docker-unpause
    "DF" 'docker-pull
    "Dk" 'docker-rm
    "Di" 'docker-images
    "Do" 'docker-stop
    "DP" 'docker-push
    "Dp" 'docker-pause
    "Dr" 'docker-restart
    "Ds" 'docker-start))

(use-package docker-tramp :defer t)

(use-package dockerfile-mode
  :defer t

  :config
  (evil-leader/set-key-for-mode dockerfile-mode
    "mcb" 'dockerfile-build-buffer))

(use-package systemd
  :if *is-a-linux*
  :defer t
  :config
  (evil-leader/set-key-for-mode systemd-mode
    "hd" 'systemd-doc-directives
    "ho" 'systemd-doc-open))

(use-package pcre2el
  :defer t

  :init
  (seartipy/declare-prefixes
    "R" "pcre2el"
    "R" "pcre2el")

  (evil-leader/set-key
    "R/"  'rxt-explain
    "Rc"  'rxt-convert-syntax
    "Rx"  'rxt-convert-to-rx
    "R'"  'rxt-convert-to-strings
    "Rpe" 'rxt-pcre-to-elisp
    "R%"  'pcre-query-replace-regexp
    "Rpx" 'rxt-pcre-to-rx
    "Rps" 'rxt-pcre-to-sre
    "Rp'" 'rxt-pcre-to-strings
    "Rp/" 'rxt-explain-pcre
    "Re/" 'rxt-explain-elisp
    "Rep" 'rxt-elisp-to-pcre
    "Rex" 'rxt-elisp-to-rx
    "Res" 'rxt-elisp-to-sre
    "Re'" 'rxt-elisp-to-strings
    "Ret" 'rxt-toggle-elisp-rx
    "Rt"  'rxt-toggle-elisp-rx))

(provide 'init-tools)
