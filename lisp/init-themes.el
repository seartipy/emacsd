(use-package gruvbox-theme :defer t)
(use-package github-theme :defer t)
(use-package darktooth-theme :defer t)
(use-package nubox :defer t)
(use-package ujelly-theme :defer t)
(use-package zerodark-theme :defer t)
(use-package cyberpunk-theme :defer t)
(use-package gotham-theme :defer t)
(use-package nord-theme :defer t)       ;; bad on terminal
(use-package darkburn-theme :defer t)
(use-package apropospriate-theme :defer t) ;; bad at paren highlighting
(use-package jazz-theme :defer t)
(use-package phoenix-dark-pink-theme :defer t)
(use-package tango-plus-theme :defer t)
(use-package moe-theme :defer t)
(use-package ample-theme :defer t)
(use-package flatland-theme :defer t) ;; bad at paren highlighting
(use-package flatui-theme :defer t)

;; (use-package all-the-icons :defer t)

(provide 'init-themes)
