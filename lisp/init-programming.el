(use-package exec-path-from-shell
  :if (not *is-a-windows*)
  :config
  (dolist (var '("SSH_AUTH_SOCK"
                 "SSH_AGENT_PID"
                 "GPG_AGENT_INFO"
                 "LANG"
                 "LC_CTYPE"
                 "JAVA_HOME"
                 "JDK_HOME"
                 "GOPATH"
                 "NVM_DIR"
                 "PYENV_ROOT"
                 "PYTHONPATH"))
    (add-to-list 'exec-path-from-shell-variables var))
  (setq exec-path-from-shell-check-startup-files nil)
  (setenv "SEARTIPY_EMACS" (emacs-version))
  (exec-path-from-shell-initialize))

(use-package eldoc
  :diminish eldoc-mode
  :defer t

  :config
  ;; enable eldoc in `eval-expression'
  (add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode))

(when (>= emacs-major-version 25)
  (eval-after-load 'bytecomp
    '(add-to-list 'byte-compile-not-obsolete-funcs
                  'preceding-sexp)))

(use-package eval-sexp-fu)



;;;; Compile

(setq-default compilation-scroll-output t)

(use-package alert :defer t)

;; Customize `alert-default-style' to get messages after compilation

(defun alert-after-compilation-finish (buf result)
  "Use `alert' to report compilation RESULT if BUF is hidden."
  (unless (catch 'is-visible
            (walk-windows (lambda (w)
                            (when (eq (window-buffer w) buf)
                              (throw 'is-visible t))))
            nil)
    (alert (concat "Compilation " result)
           :buffer buf
           :category 'compilation)))

(with-eval-after-load 'compile
  (add-hook 'compilation-finish-functions
            'alert-after-compilation-finish))

(defvar last-compilation-buffer nil
  "The last buffer in which compilation took place.")

(with-eval-after-load 'compile
  (defadvice compilation-start (after save-compilation-buffer activate)
    "Save the compilation buffer to find it later."
    (setq last-compilation-buffer next-error-last-buffer))

  (defadvice recompile (around find-prev-compilation (&optional edit-command) activate)
    "Find the previous compilation buffer, if present, and recompile there."
    (if (and (null edit-command)
             (not (derived-mode-p 'compilation-mode))
             last-compilation-buffer
             (buffer-live-p (get-buffer last-compilation-buffer)))
        (with-current-buffer last-compilation-buffer
          ad-do-it)
      ad-do-it)))

(bind-key [f6] 'recompile)

(defadvice shell-command-on-region
    (after shell-command-in-view-mode
           (start end command &optional output-buffer replace error-buffer display-error-buffer)
           activate)
  "Put \"*Shell Command Output*\" buffers into view-mode."
  (unless output-buffer
    (with-current-buffer "*Shell Command Output*"
      (view-mode 1))))

(with-eval-after-load 'compile
  (require 'ansi-color)
  (defun colourise-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  (add-hook 'compilation-filter-hook 'colourise-compilation-buffer))




(use-package flycheck-pos-tip :defer t)

(use-package flycheck
  :diminish flycheck-mode

  :init
  (add-hook 'after-init-hook 'global-flycheck-mode)
  (add-hook 'after-init-hook 'flycheck-pos-tip-mode)

  (setq flycheck-check-syntax-automatically '(save new-line mode-enabled)
        flycheck-idle-change-delay 0.8)
  (custom-set-variables
   '(flycheck-display-errors-function #'flycheck-pos-tip-error-messages))

  :config
  (evil-leader/set-key
    "ec" 'flycheck-clear
    "eh" 'flycheck-describe-checker
    "es" 'flycheck-select-checker
    "eS" 'flycheck-set-checker-executable
    "ev" 'flycheck-verify-setup))



;; Exclude very large buffers from dabbrev
(defun dabbrev-friend-buffer (other-buffer)
  (< (buffer-size other-buffer) (* 1 1024 1024)))
(setq dabbrev-friend-buffer-function 'dabbrev-friend-buffer)



(use-package company
  :diminish company-mode " ⓐ"
  :commands company-mode
  :init
  (setq company-idle-delay 0.1
        company-tooltip-limit 10
        company-minimum-prefix-length 2
        company-tooltip-flip-when-above t)

  :config
  (add-to-list 'completion-styles 'initials t)
  (setq company-minimum-prefix-length 2)

  (use-package company-quickhelp
    :defer t

    :init
    (company-quickhelp-mode 1))

  (use-package helm-company
    :bind (:map company-mode-map
                ("C-:" . helm-company)
                :map company-active-map
                ("C-:" . helm-company))))



(use-package projectile
  :defer t
  :diminish projectile-mode
  :init
  (setq-default projectile-enable-caching t)
  (setq projectile-sort-order 'recentf)
  (when *is-a-windows*
    (setq projectile-indexing-method 'alien))

  (evil-leader/set-key
    "p!" 'projectile-run-shell-command-in-root
    "p&" 'projectile-run-async-shell-command-in-root
    "pa" 'projectile-toggle-between-implementation-and-test
    "pc" 'projectile-compile-project
    "pD" 'projectile-dired
    "pG" 'projectile-regenerate-tags
    "pI" 'projectile-invalidate-cache
    "pk" 'projectile-kill-buffers
    "po" 'projectile-multi-occur
    "pR" 'projectile-replace
    "pT" 'projectile-find-test-file
    "py" 'projectile-find-tag)
  :config
  (projectile-global-mode))

(use-package helm-projectile
  :commands (helm-projectile-switch-to-buffer
             helm-projectile-find-dir
             helm-projectile-find-file
             helm-projectile
             helm-projectile-switch-project
             helm-projectile-recentf
             projectile-vc helm-projectile-grep)

  :init
  (with-eval-after-load 'projectile (helm-projectile-on))
  (evil-leader/set-key
    "pb"  'helm-projectile-switch-to-buffer
    "pd"  'helm-projectile-find-dir
    "pf"  'helm-projectile-find-file
    "ph"  'helm-projectile
    "pp"  'helm-projectile-switch-project
    "pr"  'helm-projectile-recentf
    "pv"  'projectile-vc
    "sgp" 'helm-projectile-grep))

(use-package treemacs
  :disabled (< emacs-major-version 25)
  :bind ("<f8>" . treemacs-toggle)

  :init
  (evil-leader/set-key
    "ft" 'treemacs-toggle
    "pt" 'treemacs-projectile-action))

(use-package neotree
  :disabled (>= emacs-major-version 25)
  :bind ("<f8>" . neotree-toggle)

  :init
  (evil-leader/set-key
    "ft" 'neotree-toggle
    "pt" 'neotree-projectile-action))

(use-package yasnippet
  :diminish yas-minor-mode " ⓨ"
  :commands (yas-reload-all yas-minor-mode yas-global-mode)

  :init
  (setq yas-installed-snippets-dir (concat user-emacs-directory "snippets"))
  (add-hook 'prog-mode-hook #'yas-minor-mode)
  :config
  (yas-reload-all))

(use-package auto-yasnippet :defer t)

(use-package zeal-at-point
  :if (not *is-a-mac*)
  :bind ("\C-cd" . zeal-at-point)
  :init
  (evil-leader/set-key
    "dd" 'zeal-at-point
    "dD" 'zeal-at-point-set-docset))

(use-package dash-at-point
  :defer t
  :if *is-a-mac*

  :init
  (seartipy/declare-prefix "d" "dash")
  (evil-leader/set-key
    "dd" 'dash-at-point
    "dD" 'dash-at-point-with-docset))

(use-package helm-dash
  :defer t

  :init
  (evil-leader/set-key
    "di" 'helm-dash-install-docset
    "dh" 'helm-dash-at-point
    "dH" 'helm-dash))

(use-package quickrun
  :defer t
  :init
  (evil-leader/set-key

    "cqh" 'helm-quickrun
    "cqb" 'quickrun
    "cqq" 'quickrun-region
    "cqr" 'quickrun-replace-region
    "cqa" 'quickrun-with-arg
    "cqc" 'quickrun-compile-only
    "cqs" 'quickrun-shell))

(use-package imenu
  :config
  (use-package imenu-anywhere
    :bind ("M-I" . imenu-anywhere)

    :init
    (evil-leader/set-key
      "ji" 'imenu
      "jI" 'imenu-anywhere)))

(use-package imenu-list
  :defer t
  :init
  (setq imenu-list-focus-after-activation t
        imenu-list-auto-resize t)
  (evil-leader/set-key "bi" #'imenu-list-minor-mode))

(use-package outline
  :defer t
  :diminish outline-minor-mode

  :init
  (defvar outline-minor-mode-prefix "\M-#")
  (add-hook 'prog-mode-hook 'outline-minor-mode))

(use-package outshine
  :defer t

  :init
  (setq outshine-use-speed-commands t)
  (add-hook 'outline-minor-mode-hook
            (lambda ()
              (outshine-hook-function)
              (define-key outline-minor-mode-map (kbd "M-RET") nil))))

;; (use-package multi-compile :defer t)
;; (use-package ggtags :defer t)

;; (use-package help-fns+ :defer t
;;   :init
;;   (evil-leader/set-key
;;     "hdK" 'describe-keymap))

(use-package deft
  :defer t

  :init
  (setq deft-extensions '("org" "md" "txt")
        deft-text-mode 'org-mode
        deft-use-filename-as-title t
        deft-use-filter-string-for-filename t)
  (evil-leader/set-key "an" 'deft)

  :config
  (evil-leader/set-key-for-mode deft-mode
    "d" 'deft-delete-file
    "i" 'deft-toggle-incremental-search
    "n" 'deft-new-file
    "r" 'deft-rename-file))

(provide 'init-programming)
