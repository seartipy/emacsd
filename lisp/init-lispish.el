;; (use-package paredit
;;   :defer t
;;   :diminish paredit-mode " ⓟ"
;;   :commands (enable-paredit-mode)
;;   :config
;;   (progn
;;     (suspend-mode-during-cua-rect-selection 'paredit-mode)

;;     (defun conditionally-enable-paredit-mode ()
;;       "Enable paredit during lisp-related minibuffer commands."
;;       (if (memq this-command paredit-minibuffer-commands)
;;           (enable-paredit-mode)))
;;     (add-hook 'minibuffer-setup-hook 'conditionally-enable-paredit-mode)

;;     (defvar paredit-minibuffer-commands '(eval-expression
;;                                           pp-eval-expression
;;                                           eval-expression-with-eldoc
;;                                           ibuffer-do-eval
;;                                           ibuffer-do-view-and-eval)
;;       "Interactive commands for which paredit should be enabled in the minibuffer.")


;;     (defun forward-transpose-sexps ()
;;       (interactive)
;;       (paredit-forward)
;;       (transpose-sexps 1)
;;       (paredit-backward))

;;     (defun backward-transpose-sexps ()
;;       (interactive)
;;       (transpose-sexps 1)
;;       (paredit-backward)
;;       (paredit-backward))))

(use-package smartparens
  :diminish smartparens-mode " ⓟ"
  :commands (smartparens-mode smartparens-strict-mode)

  :init
  (setq sp-show-pair-delay 0
        sp-show-pair-from-inside nil
        sp-highlight-pair-overlay nil
        sp-base-key-bindings 'paredit
        sp-autoskip-closing-pair 'always
        sp-hybrid-kill-entire-symbol nil
        sp-cancel-autoskip-on-backward-movement nil)

  :config
  (require 'smartparens-config)
  (sp-use-paredit-bindings)

  (sp-pair "(" ")" :wrap "M-(")
  (sp-pair "\"" "\"" :wrap "M-\"")
  (sp-pair "[" "]" :wrap "C-]")
  (when (display-graphic-p)
    (sp-pair "[" "]" :wrap "M-["))

  ;; (bind-key "s-<delete>" 'sp-kill-sexp smartparens-mode-map)
  ;; (bind-key "s-<backspace>" 'sp-backward-kill-sexp smartparens-mode-map)
  )

(use-package paxedit
  :defer t
  :diminish paxedit-mode

  :config
  (when (display-graphic-p)
    (bind-keys :map
               paxedit-mode-map
               ("M-<left>" . paxedit-transpose-backward)
               ("M-<right>" . paxedit-transpose-forward)
               ("M-<up>" . paxedit-backward-up)
               ("M-<down>" . paxedit-backward-end)
               ("M-b" . paxedit-previous-symbol)
               ("M-f" . paxedit-next-symbol)
               ("C-%" . paxedit-copy)
               ("C-&" . paxedit-kill)
               ("C-*" . paxedit-delete)
               ("C-^" . paxedit-sexp-raise)
               ("M-u" . paxedit-symbol-change-case)
               ("C-@" . paxedit-symbol-copy)
               ("C-#" . paxedit-symbol-kill)))
  ;; ;; Symbol backward/forward kill
  ;; (define-key paxedit-mode-map (kbd "C-w") 'paxedit-backward-kill)
  ;; (define-key paxedit-mode-map (kbd "M-w") 'paxedit-forward-kill)
  )

(use-package rainbow-delimiters :defer t)

(use-package lispy
  :defer t
  :diminish lispy-mode
  :bind (:map
         lispy-mode-map
         ("M-]" . lispy-forward)
         ("M-}" . lispy-backward))

  :preface
  (defun seartipy/enable-paxedit-mode ()
    (when (display-graphic-p))
    (paxedit-mode))

  :config
  (if seartipy-lispy
      (lispy-set-key-theme '(special c-digits parinfer))
    (lispy-set-key-theme '(parinfer)))

  (key-chord-define lispy-mode-map  "]]" (kbd "M-]"))
  (key-chord-define lispy-mode-map "[[" (kbd "M-}")))

(use-package aggressive-indent :defer t)

(add-hook 'emacs-lisp-mode-hook
          '(lambda ()
             (aggressive-indent-mode)
             (company-mode)
             (rainbow-delimiters-mode)
             (smartparens-strict-mode)
             (lispy-mode)
             (seartipy/enable-paxedit-mode)))

(provide 'init-lispish)
