;;;; fonts

;; (defun seartipy/set-font (font)
;;   "Set the FONT as frame font, if the font is available."
;;   (let ((font-name (apply #'concat (-interpose " " (butlast (split-string font))))))
;;     (if (-contains? (font-family-list) font-name)
;;         (add-to-list 'default-frame-alist (cons 'font font))
;;       (message (format "%s not available on this system" font-name)))))

;; (when *is-a-mac*
;;   (seartipy/set-font seartipy-mac-font))

;; (when *is-a-linux*
;;   (seartipy/set-font seartipy-linux-font))

;; (when *is-a-windows*
;;   (seartipy/set-font seartipy-windows-font))

(defun seartipy/font-available-p (font)
  "Check if FONT available on the system."
  (-contains? (font-family-list) font))

(-when-let (font (-first 'seartipy/font-available-p
                         seartipy-fonts))
  (set-frame-font (format "%s %d" font seartipy-font-size)))



;;;; themes
(use-package helm-themes :defer t)

(use-package spacemacs-theme :defer t)
(use-package dracula-theme :defer t)
(use-package solarized-theme :defer t)
(use-package monokai-theme :defer t)
(use-package material-theme :defer t)
(use-package leuven-theme :defer t)
(use-package darkokai-theme :defer t)
(use-package color-theme-sanityinc-tomorrow :defer t)

(setq darkokai-mode-line-padding 1) ;; Default mode-line box width
(load-theme seartipy-theme t)



;; must be loaded after most things

(use-package spaceline
  :init
  (defadvice load-theme (after reset-powerline-with-eval-after-load-theme activate)
    (powerline-reset))

  (setq-default spaceline-window-numbers-unicode t)
  (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
  (setq spaceline-display-default-perspective nil)
  (setq spaceline-byte-compile t)

  :config
  (require 'spaceline-config)
  (spaceline-helm-mode)
  ;; (spaceline-spacemacs-theme)
  (spaceline-install
    '(((persp-name
        window-number)
       :fallback evil-state
       :separator "|"
       :face highlight-face)
      ((point-position
        line-column)
       :separator " | ")
      (buffer-modified buffer-id remote-host)
      anzu
      major-mode
      ((flycheck-error flycheck-warning flycheck-info)
       :when active)
      (((minor-modes :separator spaceline-minor-modes-separator)
        process)
       :when active)
      projectile-root
      (version-control :when active))

    `(selection-info
      (global :when active)
      buffer-position))
  (setq-default mode-line-format '("%e" (:eval (spaceline-ml-main))))
  (add-hook 'after-init-hook 'spaceline-compile))

(provide 'init-ui)
