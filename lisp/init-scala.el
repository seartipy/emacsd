;;;; scala

(use-package scala-mode
  :interpreter
  ("scala" . scala-mode)
  :bind (:map scala-mode-map
              ("<backtab>" . scala-indent:indent-with-reluctant-strategy))
  :init
  (setq scala-indent:align-parameters t
        scala-indent:align-forms t
        scala-indent:indent-value-expression nil)

  :config
  (with-eval-after-load 'smartparens-mode-hook
    (lambda ()
      ;; (bind-key "s-{" 'sp-rewrap-sexp smartparens-mode-map)
      (sp-local-pair 'scala-mode "(" nil :post-handlers '(("||\n[i]" "RET")))
      (sp-local-pair 'scala-mode "{" nil :post-handlers '(("||\n[i]" "RET") ("| " "SPC")))))

  (setq scala-indent:default-run-on-strategy scala-indent:eager-strategy))

(use-package sbt-mode
  :commands sbt-start sbt-command

  :init
  (add-hook 'scala-mode-hook
            (lambda ()
              (require 'sbt-mode)
              (scala-mode:goto-start-of-code)
              (prettify-symbols-mode)
              (show-paren-mode)))

  (add-hook 'sbt-mode-hook
            '(lambda ()
               (local-set-key (kbd "C-a") 'comint-bol)
               (local-set-key (kbd "M-RET") 'comint-accumulate)
               ))
  (add-hook 'sbt-mode-hook
            (lambda ()
              (setq prettify-symbols-alist
                    `((,(expand-file-name (directory-file-name default-directory)) . ?⌂)
                      (,(expand-file-name "~") . ?~)))
              (prettify-symbols-mode t)))
  ;; (add-hook 'scala-mode-hook
  ;;           '(lambda ()
  ;;              (local-set-key (kbd "M-.") 'sbt-find-definitions)
  ;;              (local-set-key (kbd "C-x '") 'sbt-run-previous-command)))
  :config
  (substitute-key-definition 'minibuffer-complete-word
                             'self-insert-command
                             minibuffer-local-completion-map))

(use-package ensime
  :commands (ensime ensime-mode)
  :init
  (setq ensime-startup-snapshot-notification nil
        ensime-startup-notification nil
        ensime-sbt-perform-on-save "compile"
        ensime-eldoc-hints 'error

        ensime-search-interface 'helm
        ensime-save-before-compile t
        ensime-kill-without-query-p t


        ensime-tooltip-type-hints t
        ensime-tooltip-hints t
        ensime-graphical-tooltips t

        ensime-auto-generate-config nil)

  (setq ensime-sem-high-enabled-p nil)

  ;; Don't use scala checker if ensime mode is active,
  ;; since it provides better error checking.
  (with-eval-after-load 'flycheck
    (add-hook 'ensime-mode-hook (lambda ()
                                  (flycheck-mode -1))))

  :config
  (with-eval-after-load 'expand-region
    (require 'ensime-expand-region))
  (setq prettify-symbols-alist scala-prettify-symbols-alist)
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map))

(seartipy/declare-prefixes-for-mode 'scala-mode
  "b" "build"
  "c" "check"
  "d" "debug"
  "e" "errors"
  "g" "goto"
  "h" "docs"
  "i" "inspect"
  "n" "ensime"
  "r" "refactor"
  "s" "repl"
  "t" "test")

(evil-leader/set-key-for-mode scala-mode
  "/"     'ensime-search

  "bc"     'ensime-sbt-do-compile
  "bC"     'ensime-sbt-do-clean
  "bi"     'ensime-sbt-switch
  "bp"     'ensime-sbt-do-package
  "br"     'ensime-sbt-do-run

  "ct"     'ensime-typecheck-current-buffer
  "cT"     'ensime-typecheck-all

  "dA"     'ensime-db-attach
  "db"     'ensime-db-set-break
  "dB"     'ensime-db-clear-break
  "dC"     'ensime-db-clear-all-breaks
  "dc"     'ensime-db-continue
  "di"     'ensime-db-inspect-value-at-point
  "dn"     'ensime-db-next
  "do"     'ensime-db-step-out
  "dq"     'ensime-db-quit
  "dr"     'ensime-db-run
  "ds"     'ensime-db-step
  "dt"     'ensime-db-backtrace

  "ee"     'ensime-print-errors-at-point
  "el"     'ensime-show-all-errors-and-warnings
  "es"     'ensime-stacktrace-switch

  "gg"     'ensime-edit-definition
  "gp"     'ensime-pop-find-definition-stack
  "gi"     'ensime-goto-impl
  "gt"     'ensime-goto-test

  "hh"     'ensime-show-doc-for-symbol-at-point
  "hu"     'ensime-show-uses-of-symbol-at-point
  "ht"     'ensime-print-type-at-point

  "ii"     'ensime-inspect-type-at-point
  "iI"     'ensime-inspect-type-at-point-other-frame
  "ip"     'ensime-inspect-project-package

  "nf"     'ensime-reload
  "nF"     'ensime-reload-open-files
  "ns"     'ensime
  "nq"     'ensime-shutdown
  "nS"     'ensime-refresh-config

  "rd"     'ensime-refactor-diff-inline-local
  "rD"     'ensime-undo-peek
  "rf"     'ensime-format-source
  "ri"     'ensime-refactor-diff-organize-imports
  "rm"     'ensime-refactor-diff-extract-method
  "rr"     'ensime-refactor-diff-rename
  "rt"     'ensime-import-type-at-point
  "rv"     'ensime-diff-refactor-extract-local

  "ta"     'ensime-sbt-do-test-dwim
  "tr"     'ensime-sbt-do-test-quick-dwim
  "tt"     'ensime-sbt-do-test-only-dwim

  "sa"     'ensime-inf-load-file
  "sb"     'ensime-inf-eval-buffer
  "si"     'ensime-inf-switch
  "sr"     'ensime-inf-eval-region

  "z"      'ensime-expand-selection-command)

(provide 'init-scala)
