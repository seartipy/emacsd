;; (package-initialize)

;; https://bling.github.io/blog/2016/01/18/why-are-you-changing-gc-cons-threshold/
(defconst kilobyte 1000)
(defconst megabyte (* kilobyte kilobyte))
(defconst default-gc-cons-threshold gc-cons-threshold)

(defun seartipy/minibuffer-setup-hook ()
  (setq gc-cons-threshold (* 500 megabyte)))

(defun seartipy/minibuffer-exit-hook ()
  (setq gc-cons-threshold default-gc-cons-threshold))

(add-hook 'minibuffer-setup-hook #'seartipy/minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'seartipy/minibuffer-exit-hook)

(let ((gc-cons-threshold (* 500 megabyte)))
  (add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
  (add-to-list 'load-path (expand-file-name "vendor" user-emacs-directory))
  (add-to-list 'load-path (expand-file-name "vendor/cc-mode-5.33" user-emacs-directory))

  (require 'init-default-settings)
  (require 'init-melpa)

  (require 'init-config)

  (when (file-exists-p "~/.emacs-pre-local.el")
    (load-file "~/.emacs-pre-local.el"))

  (require 'init-keys)
  (require 'init-essential)
  (require 'init-save)
  (require 'init-helm)

  (require 'init-editing)
  (require 'init-editing-visual)
  (require 'init-git)
  (require 'init-search)
  (require 'init-keybindings)

  (require 'init-locale)
  (require 'init-layers)

  (when (file-exists-p "~/.emacs-post-local.el")
    (load-file "~/.emacs-post-local.el")))

;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
