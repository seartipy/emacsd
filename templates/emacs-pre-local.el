(setq-default

 ;; Select the layers you want enabled from
 ;; `clojure'
 ;; `scala'
 ;; `python'
 ;; `haskell'
 ;; `web'
 ;; `cpp'
 ;; `shell'
 ;; `elisp'
 ;; `ui'
 ;; `term'
 ;; `tools'
 ;; `themes'
 ;; `os'
 ;; `markdown'
 ;;
 seartipy-layers '(ui term)

 ;;  The first available font, will be set
 seartipy-fonts '("Consolas"
                  "Monaco"
                  "Ubuntu Mono"
                  "Roboto Mono"
                  "Source Code Pro"
                  "mononoki"
                  "Inconsolata"
                  "Fira Code"
                  "Hack"
                  "Dejavu Sans Mono")

 ;; Default font size"
 seartipy-font-size 13

 ;; Select one of the themes from
 ;; `material',
 ;; `material-light',
 ;; `spacemacs-dark',
 ;; `spacemacs-light',
 ;; `solarized-dark',
 ;; `solarized-light'
 ;; `dracula'
 ;; `monokai'
 ;; `darkokai'
 ;; solarized does not work well in terminal.
 seartipy-theme 'darkokai

 ;; If not nil, spacemacs like dashboard is shown at emacs start
 seartipy-dashboard nil
 )
